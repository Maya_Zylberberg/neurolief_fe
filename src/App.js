import React, {Component} from 'react'
import {Route, Router, Switch} from 'react-router-dom'
import  { Redirect } from 'react-router-dom'
import {authenticationService} from "./services/AuthenticationService";
import Login from './views/default/Login';
import Main from './views/default/Main';
import {history} from './utils/history';
import Idle from 'react-idle'
import NotFound from "./views/Error/NotFound";
import './App.css';
import './app_rtl.css'
/*import PatientForm from "./views/eye_care_provider/PatientForm";
import Patient from "./views/eye_care_provider/Patient";
import EditPatient from "./views/eye_care_provider/EditPatient";*/
import NovaNavbar from "./components/single_components/NovaNavbar";
import ResetPassword from "./views/default/ResetPassword";
import Login2FA from "./views/default/Login2FA";
import CreatePassword from "./views/default/CreatePassword";
import SingleRegistration from "./views/default/SingleRegistration";
// import TabsDashboard from "./views/eye_care_provider/PatientTabs/TabsDashboard";
import PatientInfoPage from './views/eye_care_provider/doctor-tabs/doctorComponents/patientInfoParts/PatientInfoPage'


class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentUser: null,
            navbarCollapsed: true,
        }
    }

    componentDidMount = async ()  => {
        authenticationService.currentUser.subscribe(
            x => this.setState(
                {
                    currentUser: x
                })
        );
        history.push('/login')
    }
    handleLogout = async () => {

        authenticationService.logout();
        history.push('/login')
    };

    render() {
        let {currentUser} = this.state
        return (

            <Router history={history}>
                <Idle timeout='3600000' onChange={({idle}) => idle ? this.handleLogout() : console.log(idle)}
                      render={() =>
                          <div className={'nova-app-container'}>
                              {currentUser && (<><NovaNavbar currentUser={currentUser}/><div className={'width-separator'} /></>)}
                              <Switch>
                                  {/*<Route path="/dashboard/Patient" component={Patient}/>
                                  <Route path="/dashboard/AddPatient" component={PatientForm}/>
                                  <Route path="/dashboard/EditPatient" component={EditPatient}/>*/}

                                  <Route path="/dashboard/Patient" component={PatientInfoPage}/>
                                  <Route path="/dashboard" component={Main}/>
                                  <Route path="/login" component={Login}/>
                                  <Route path="/2FA" component={Login2FA}/>
                                  <Route path="/create-password" component={CreatePassword}/>
                                  <Route path="/ResetPassword" component={ResetPassword}/>

                                  <Route path="/register" component={SingleRegistration}/>
                                  <Route path="/NotFound" component={NotFound}/>
                                  <Redirect to='/NotFound' />
                              </Switch>
                          </div>
                      }>
                </Idle>
            </Router>
        )
    }
}

export default App;
