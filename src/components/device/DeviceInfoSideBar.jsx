import React, {Component} from "react";
import PropTypes from "prop-types";
import utils from "../../utils/utils";
import Prototypes from "prop-types";
import SvGraphics from "../../assets/SvGraphics";
import MyTable from "../NovaTable/Table";


class DeviceInfoSideBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deviceInfo: null
        };
    }
    setTempData = () => {
        let data = [
            {
                PatientName: "Maya Zylberberg",
                DeviceSN: 323443092,
                Sites: "dfggg",
                ActivationLabel: "24",
            },
            {
                PatientName: "subhi samara",
                DeviceSN: 123456789,
                Sites: "ggddsg",
                ActivationLabel: "28",
            },
            {
                PatientName: "subhi2 samara2",
                DeviceSN: 987654321,
                Sites: "kjgfh",
                ActivationLabel: "22",

            },
            {
                PatientName: "Maya Zylberberggg",
                DeviceSN: 564443092,
                Sites: "Feffnale",
                ActivationLabel: "26",

            },]
        this.props.setPatientsData(data)

    }

    async componentWillMount(){
        if (this.props && this.props.deviceInfo) await this.setState({deviceInfo:this.props.deviceInfo})
    }
    //
    // componentDidMount() {
    //     this.setTempData()
    // }
    async closeSideBar(){
        let {t} = this.props
        document.body.style.overflow = "auto"
        this.props.closeSideBar(false)
        this.setState({entry:null})
    }
    getFields = (t) => {
        return [
            {accessor: 'PatientName', Header: 'Patient Name', resizable: false},
            {accessor: 'DeviceSN', Header: t('admin.device.table.device_sn'), resizable: false},
            {accessor: 'Sites', Header: 'Sites', resizable: false},
            {accessor: 'ActivationLabel', Header: 'ActivationLabel', resizable: false}
            ]
    }

    render() {
        let display = this.props.isSideBarOpen ? 'block' : 'none'
        let width = this.props.isSideBarOpen ? "800px" : "0px"
        let container_width = this.props.isSideBarOpen ? "100%" : "0px"
        let {t} = this.props
        let AllowedFields = this.getFields(t)
        return (
            <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}}>
                <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} onClick={this.closeSideBar.bind(this)}/>
                <div className={"sidenav"} style={{width:width, height:"100%", float:'right'}}>
                    <SvGraphics onClick={this.closeSideBar.bind(this)} className={'close-bar-x'} svgname={'close'} height={'15px'} width={'15px'} style={{
                        float: 'left',
                        top: '10px',
                        left: '10px',
                        position: 'absolute',
                        cursor: 'pointer'
                    }}/>
                    <div className={"adminMargin"}>

                        <div className={'deviceBarFont'}> Patient's details</div>

                        <div className={'deviceBarSmallFont'}>Parent 1 </div>
                        <div className={'patientsFormBoxes'}>
                            <div className={'adminInputSlots2'}>
                                <label>Phone number*</label>
                                <input type="phone"  name="phoneNumber"/>
                            </div>
                            <div className={'adminInputSlots2'}>
                                <label>Site*</label>
                                <input type="text"  name="ID"/>
                            </div>
                        </div>
                        <div className={'deviceBarSmallFont'}>Settings </div>
                        <div className={'patientsFormBoxes'}>
                            <div className={'adminInputSlots2'}>
                                <label>Device Size</label>
                                <input type="number"  name="deviceSize"/>
                            </div>

                        </div>
                        <div className={'deviceBarSmallFont'}>Info </div>
                        <div className={'patientsFormBoxes'}>
                            <div className={'adminInputSlots2'}>
                                <label>Usage hours</label>
                                <input type="number"  name="UsageHours"/>
                            </div>
                            <div className={'adminInputSlots2'}>
                                <label>Account Status</label>
                                <input type="text"  name="ID"/>
                            </div>
                        </div>
                        <div className={'deviceBarSmallFont'}>logs </div>
                            {/*<MyTable*/}
                            {/*    // data={this.state.isMore ? this.props.patientsData : this.props.patientsData.slice(0,8)}*/}
                            {/*    data={ this.props.patientsData}*/}
                            {/*    initialData=/!*this.props.initiateVersionData*/[]}*/}
                            {/*    columns={AllowedFields}*/}
                            {/*/>*/}

                        {/* <div className={'deviceBarSmallFont'}>Settings</div>
                        <div className={'deviceBarInsideFont'}> Device Size</div>
                        <div className={'deviceBarInsideFont'}>Next Training Date</div>

                        <div className={'deviceBarSmallFont'}>Info </div>
                        <div className={'deviceBarInsideFont'}>Usage hours</div>
                        <div className={'deviceBarInsideFont'}> Account Status</div>
*/}

                        <button className={'DevicesBarButton'}>Save</button>
                    </div>
                </div>
            </div>
        );
    }
}
DeviceInfoSideBar.propTypes = {
    id:PropTypes.bool,
    isSideBarOpen:Prototypes.bool.isRequired,
    closeSideBar:Prototypes.func.isRequired,
}

export default DeviceInfoSideBar

