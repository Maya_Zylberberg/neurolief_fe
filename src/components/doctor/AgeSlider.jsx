import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';

class AgeSlider extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            value: [0,100]
        }
    }

    valuetext(value) {
        return `${value}`;
    }

    componentWillMount() {
        if (this.props.sliderVal === []) {
            this.props.handleSliderChange([0,100])
            this.setState({value:[0,100]});
        } else
        this.setState({value:this.props.sliderVal});
    }


    handleChange = (event, newValue) => {
        this.setState({value:newValue});
        this.props.handleSliderChange(newValue)
    };

    render() {
        return (
            <div >
                <Slider
                    // min={0}
                    // max={100}
                    value={this.state.value}
                    // defaultValue={this.props.sliderVal}
                    onChange={this.handleChange}
                    valueLabelDisplay="auto "
                    aria-labelledby="ios slider"
                    getAriaValueText={this.valuetext}
                />
            </div>
        );
    }

}

const useStyles = makeStyles({
    root: {
        width: 300,
    },
});

function valuetext(value) {
    return `${value}`;
}

export default AgeSlider