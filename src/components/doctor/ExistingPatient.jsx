import React, {Component} from "react";
import {BsX,BsChevronLeft} from 'react-icons/bs';
import Constants from "../../utils/constants";
import {HiOutlineChevronLeft} from 'react-icons/hi';

import {compose} from "redux";
import {withTranslation} from "react-i18next";
import ExistingPhoneConfirmation from "./ExistingPhoneConfirmation";





class ExistingPatientComposed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sendPhoneClicked: false
        };
    }


    async closeSideBar(){
        document.body.style.overflow = "auto"
        this.props.closeSideBar(false)
        this.setState({entry:null})
    }

    async sendPhone(){
        document.body.style.overflow = "hidden"
        this.setState({sendPhoneClicked:true})
    }


    render() {
        let display = this.props.isExistingPatientOpen ? 'block' : 'none'
        let width = this.props.isExistingPatientOpen ? "600px" : "0px"
        let container_width = this.props.isExistingPatientOpen ? "100%" : "0px"
        let {t} = this.props
        let {sendPhoneClicked} = this.state


        return (
            <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} >
                <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} onClick={this.closeSideBar.bind(this)}/>
                <div className={"sidenav"} style={{width:width, height:"100%", float:'right'}}>
                    <BsX onClick={this.closeSideBar.bind(this)} size={35} style={{
                        float: 'left',
                        top: '10px',
                        left: '10px',
                        position: 'absolute',
                    }}/>
                    <div className={"existingPatientMargins"}>
                        <div className={'filterFont'}> <HiOutlineChevronLeft onClick={this.closeSideBar.bind(this)} size={28}/> {t('doctor.existingPatient.existingPatient')}</div>
                            {/*<div className={'existingPatientContent'}>*/}

                            <h6>
                                In order to add the patient to your clinic,
                                please send a verification message to the patient</h6>
                        {/*</div>*/}

                        <div className={'phoneArea'}>
                            Patient's Phone Number
                            <div className={'phoneInsertionBox'}/>
                        </div>

                        <div className={'ExistingSendButton'} onClick={this.sendPhone.bind(this)}> send </div>

                    </div>
                </div>
                  {sendPhoneClicked && <ExistingPhoneConfirmation
                    title={'Edit Admin'}
                    sendPhoneClicked={sendPhoneClicked}
                    closeSideBar={() => this.setState({sendPhoneClicked: false})}/>}
            </div>
        );
    }
}


const ExistingPatient = compose(
    withTranslation(),
    /*connect(
        mapStateToProps,
        mapDispatchToProps
    )*/
)(ExistingPatientComposed);

export default ExistingPatient;
