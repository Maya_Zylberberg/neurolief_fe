import React, {Component} from "react";
import {BsX,BsChevronLeft} from 'react-icons/bs';
import Constants from "../../utils/constants";
import {HiOutlineChevronLeft} from 'react-icons/hi';

import {compose} from "redux";
import {withTranslation} from "react-i18next";
import {history} from "../../utils/history";





class ExistingPhoneConfirmationComposed extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }


    async closeSideBar(){
        document.body.style.overflow = "auto"
        this.props.closeSideBar(false)
        this.setState({entry:null})
    }
    async openExistingPatient(){

        history.push({
            pathname: '/dashboard/Patient',
            state: {
/*
                Email:this.state.username,
*/
                from: {
                    fromPath: `/dashboard`,
                }
            },
        })
    }


    render() {
        let display = this.props.sendPhoneClicked ? 'block' : 'none'
        let width = this.props.sendPhoneClicked ? "600px" : "0px"
        let container_width = this.props.sendPhoneClicked ? "100%" : "0px"
        let {t} = this.props
        let {sendPhoneClicked} = this.state


        return (
            <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} >
                <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} onClick={this.closeSideBar.bind(this)}/>
                <div className={"sidenav"} style={{width:width, height:"100%", float:'right'}}>
                    <BsX onClick={this.closeSideBar.bind(this)} size={35} style={{
                        float: 'left',
                        top: '10px',
                        left: '10px',
                        position: 'absolute',
                    }}/>
                    <div className={"existingPatientMargins"}>
                        <div className={'filterFont'}>  {t('doctor.existingPatient.existingPatient')}</div>
                        <h6>
                            We've sent a verification message to your patient. Once the patient will verify, you will be transferred to the medical questionnaire.
                        </h6>
                        {/*<div className={'ExistingSendButton'} onClick={this.openExistingPatient}> ok </div>*/}

                    </div>
                </div>
                {/*  {isNewPatientOpen && <AddPatient
                    title={'Edit Admin'}
                    isNewPatientOpen={isNewPatientOpen}
                    closeSideBar={() => this.setState({isSideBarOpen: false})}/>}
                    */}

            </div>
        );
    }
}


const ExistingPhoneConfirmation = compose(
    withTranslation(),
    /*connect(
        mapStateToProps,
        mapDispatchToProps
    )*/
)(ExistingPhoneConfirmationComposed);

export default ExistingPhoneConfirmation;
