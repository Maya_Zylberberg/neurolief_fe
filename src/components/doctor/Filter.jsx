import React, {Component} from "react";
import '../../components_style/doctorComponents/RegisterForm.css'
// import PropTypes from "prop-types";
// import SimpleFormBuilder from "../forms_builder/SimpleFormBuilder";
// import Prototypes from "prop-types";
// import {adminApi} from "../../services/ApiService";
import * as Yup from "yup";
// import {makeError} from "../modals/ErrorModal";
import Constants from "../../utils/constants";
// import AlertConfirm from "../modals/Confirm";
// import AllPatients from "../../views/Provider/doctor-tabs/AllPatients";
/*
import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';
*/
// import Slider from '@material-ui/core/Slider';
import { MdExpandLess, MdExpandMore } from 'react-icons/md';
import {BsX} from 'react-icons/bs';
import AgeSlider from "./AgeSlider";
import { AiOutlineCheckCircle } from 'react-icons/ai';
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import {connect} from "react-redux";
import {filterPatientsTable} from "../../redux/actions/Tabs-Actions";




class FilterComposed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entry: {},
            matrix :[2,2,1,1],
            fields : [
                {type:'text',max: 20, name:'FirstName',display:'First name', placeholder: 'Enter first name', required:true, width:0.5, style:'inline'},
                {type:'text',max: 20, name:'LastName',display:'Last name', placeholder: 'Enter last name', required:true, width:0.5, style:'inline'},
                {type:'phone', max: 50, name:'PhoneNumber1',display:'Phone number', placeholder: 'Enter Phone number', required:true, width:0.5, style:'inline'},
                {type:'dropdown', name:'Role',display:'Role', placeholder: 'Enter Role', required:false, width:0.5, style:'inline', options: Constants.adminRoles},
                {type:'text',max: 100, name:'Email1',display:'Email', placeholder: 'Enter email', required:true, width:1, style:'inline'},
                {type:'checkbox', name:'TwoFA',display:'is 2FA required', width:1, style:'block', required: false},
            ],
            errors: {},
            errorsOnSave:{},
            StatusExpand: MdExpandLess,
            DiagnosisExpand: MdExpandLess,
            TreatmentExpand: MdExpandLess,
            SexExpand: MdExpandLess,
            AgeExpand: MdExpandLess,

            // domain: [200, 500],
            // values: defaultValues.slice(),
            // update: defaultValues.slice(),
            // reversed: false,

            filters: {
                Medications:[],
                lastVisitFrom: null,
                lastVisitTo: null,
                startFrom: null,
                startTo: null,
                // Diagnosis: [],
                // Treatment: [],
                Sex: [],
                Age: [0,100],
            },
        };
    }

    componentWillMount() {
        if (this.props.filters)
            this.setState({filters: this.props.filters})

        Object.entries(this.props.filters).forEach(([name,value]) => {

        })
    }


    async closeSideBar(){
        document.body.style.overflow = "auto"
        this.props.closeSideBar(false)
        this.setState({entry:null})
    }
 /*   changeCheckbox = (name) => {
        let state = this.state
        let val = !state[name]
        state[name] = val
        this.setState( state)
    }*/

    applyFilter(name,cat,field){
        let {filters} = this.state
        let catArr = filters[cat]
        let fieldName = field;
        if (!catArr.includes(fieldName)){
            catArr.push(fieldName);
        }
        else {
            catArr.splice(fieldName)
        }
        filters[cat] = catArr
        this.setState({filters});
        this.props.filterPatientsTable('filters', filters);
    }

    resetFilters(){
        let filters = {
            Medications: [],lastVisitFrom: null,
            lastVisitTo: null,
            startFrom: null,
            startTo: null, Sex: [], Age: [0, 100]
        }
        document.getElementById('lastVisitFrom').value = ""
        document.getElementById('lastVisitTo').value = ""
        document.getElementById('startFrom').value = ""
        document.getElementById('startTo').value = ""
        this.setState({filters});
        this.props.filterPatientsTable('filters', filters);
    }

    handleSliderChange(cat,valArr){
        let {filters} = this.state
        filters[cat] = valArr
        this.setState({filters});
        this.props.filterPatientsTable('filters', filters);
    }

    expandFilter(id){
        let display = document.getElementById(id).style.display

        if (display === "block") display = "none"
        else display = "block"

        switch (id){
            case "medication-panel":{
                if (display === "block"){
                    this.setState({StatusExpand: MdExpandLess})
                } else {
                    this.setState({StatusExpand: MdExpandMore})
                }
                break;
            }
            case "startDate-panel":{
                if (display === "block"){
                    this.setState({DiagnosisExpand: MdExpandLess})
                } else {
                    this.setState({DiagnosisExpand: MdExpandMore})
                }
                break;
            }
            // case "lastVisit-panel":{
            //     if (display === "block"){
            //         this.setState({TreatmentExpand: MdExpandLess})
            //     } else {
            //         this.setState({TreatmentExpand: MdExpandMore})
            //     }
            //     break;
            // }
            case "Sex-panel":{
                if (display === "block"){
                    this.setState({SexExpand: MdExpandLess})
                } else {
                    this.setState({SexExpand: MdExpandMore})
                }
                break;
            }
            case "Age-panel":{
                if (display === "block"){
                    this.setState({AgeExpand: MdExpandLess})
                } else {
                    this.setState({AgeExpand: MdExpandMore})
                }
                break;
            }
        }
        document.getElementById(id).style.display = display;
    }

    render() {
        let display = this.props.isSideBarOpen ? 'block' : 'none'
        let width = this.props.isSideBarOpen ? "600px" : "0px"
        let container_width = this.props.isSideBarOpen ? "100%" : "0px"
        let {StatusExpand,DiagnosisExpand,TreatmentExpand,SexExpand,AgeExpand} = this.state

        let {filters} = this.state
        let {t} = this.props

        return (
            <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} >
                <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} onClick={this.closeSideBar.bind(this)}/>
                <div className={"sidenav"} style={{width:width, height:"100%", float:'right'}}>
                    <BsX onClick={this.closeSideBar.bind(this)} size={35} style={{
                        float: 'left',
                        top: '10px',
                        left: '10px',
                        position: 'absolute',
                    }}/>
                    <div className={'filterFont'}> {t('doctor.filter.filter')}</div>
                    <div className={'filterBorders'}>

                        <div className={'filterTypes'}>
                            <div className={'filterTypes-header'}>
                                <div className={'filterTypes-header-title pointer'} onClick={() => this.expandFilter('startDate-panel')}> {t('doctor.filter.TreatmentStartDate')}</div>
                                <DiagnosisExpand className={'expandingIcon pointer'} size={35} onClick={() => this.expandFilter('startDate-panel')}/>
                            </div>
                            <div id={'startDate-panel'} style={{display: "block"}}>
                                <div className={'displayFlex'}>
                                    <div className={'filterInputSlots'}>
                                        <label className={'labelStyle'}>{t('doctor.filter.from')} </label>
                                        <input className={'filterDateSlots'} id={'startFrom'} type="date" name="startFrom" onChange={e => this.handleSliderChange(e.target.name,e.target.value)}/>
                                    </div>
                                    <div className={'filterInputSlots'}>
                                        <label className={'labelStyle'}>{t('doctor.filter.to')}</label>
                                        <input className={'filterDateSlots'} id={'startTo'} type="date"  name="startTo" onChange={e => this.handleSliderChange(e.target.name,e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={'filterTypes'}>
                            <div className={'filterTypes-header'}>
                                <div className={'filterTypes-header-title pointer'} onClick={() => this.expandFilter('lastVisit-panel')}>{t('doctor.filter.LastVisit')} </div>
                                <TreatmentExpand className={'expandingIcon pointer'} size={35} onClick={() => this.expandFilter('lastVisit-panel')}/>
                            </div>
                            <div className={'displayFlex'} id={'lastVisit-panel'} style={{display: "block"}}>
                                <div className={'displayFlex'}>
                                    <div className={'filterInputSlots'}>
                                        <label className={'labelStyle'}>{t('doctor.filter.from')} </label>
                                        <input className={'filterDateSlots'} id={'lastVisitFrom'} type="date"  name="lastVisitFrom" onChange={e => this.handleSliderChange(e.target.name,e.target.value)}/>
                                    </div>
                                    <div className={'filterInputSlots'}>
                                        <label className={'labelStyle'}>{t('doctor.filter.to')} </label>
                                        <input className={'filterDateSlots'} id={'lastVisitTo'} type="date"  name="lastVisitTo" onChange={e => this.handleSliderChange(e.target.name,e.target.value)}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={'filterTypes'}>
                            <div className={'filterTypes-header'}>
                                <div className={'filterTypes-header-title pointer'} onClick={() => this.expandFilter('medication-panel')}> {t('doctor.filter.Medications')} </div>
                                <StatusExpand className={'expandingIcon pointer'} size={35} onClick={() => this.expandFilter('medication-panel')}/>
                            </div>
                            <div id={'medication-panel'} style={{display: "block"}}>
                                <div className={'filterTypes-header'}>
                                    <div className={'filterOptions filterTypes-header-title'}
                                         style={{fontWeight: filters.Medications.includes('Opioids') ? 'bold' : 'normal'}}
                                         onClick={() => this.applyFilter('OpioidsClicked', 'Medications','Opioids')}>  {t('doctor.filter.Opioids')}  </div>
                                    <AiOutlineCheckCircle style={{display: filters.Medications.includes('Opioids')?'inline-block': 'none'}} className={'expandingIcon'} size={18} />
                                </div>
                                <div className={'filterTypes-header'}>
                                    <div className={'filterOptions filterTypes-header-title'}
                                         style={{fontWeight: filters.Medications.includes('Tritans') ? 'bold' : 'normal'}}
                                         onClick={() => this.applyFilter('TritansClicked','Medications','Tritans')}> {t('doctor.filter.Tritans')}  </div>
                                    <AiOutlineCheckCircle style={{display: filters.Medications.includes('Tritans') ?'inline-block': 'none'}} className={'expandingIcon'} size={18} />
                                </div>
                            </div>
                        </div>
                        <div className={'filterTypes'}>
                            <div className={'filterTypes-header'}>
                                <div className={'filterTypes-header-title pointer'} onClick={() => this.expandFilter('Sex-panel')}>{t('doctor.filter.sex')}</div>
                                <SexExpand className={'expandingIcon pointer'} size={35} onClick={() => this.expandFilter('Sex-panel')}/>
                            </div>
                            <div id={'Sex-panel'} style={{display: "block"}}>
                                <div className={'filterTypes-header'}>
                                    <div className={'filterOptions filterTypes-header-title'}
                                         style={{fontWeight: filters.Sex.includes('Male') ? 'bold' : 'normal'}}
                                         onClick={() => this.applyFilter('maleLicenseClicked','Sex','Male')}>  {t('doctor.filter.male')} </div>
                                    <AiOutlineCheckCircle style={{display: filters.Sex.includes('Male') ?'inline-block': 'none'}} className={'expandingIcon'} size={18} />
                                </div>
                                <div className={'filterTypes-header'}>
                                    <div className={'filterOptions filterTypes-header-title'}
                                         style={{fontWeight: filters.Sex.includes('Female')  ? 'bold' : 'normal'}}
                                         onClick={() => this.applyFilter('femaleClicked','Sex','Female')}>  {t('doctor.filter.female')} </div>
                                    <AiOutlineCheckCircle style={{display: filters.Sex.includes('Female')?'inline-block': 'none'}} className={'expandingIcon'} size={18} />
                                </div>
                            </div>
                        </div>
                        <div className={'filterTypes'}>
                            <div className={'filterTypes-header'}>
                                <div className={'filterTypes-header-title pointer'} onClick={() => this.expandFilter('age-panel')}> {t('doctor.filter.age')}</div>
                                <AgeExpand className={'expandingIcon pointer'} size={35} onClick={() => this.expandFilter('age-panel')}/>
                            </div>
                            <div id={'age-panel'} style={{display: "block"}}>
                                <AgeSlider style={{color: 'black'}} sliderVal={(filters['Age']).length === 2 ? filters['Age'] : [0,100]} handleSliderChange={valArr => this.handleSliderChange('Age',valArr)}/>
                            </div>
                        </div>
                        <div className={'filterButtonWrapper'}>
                            <div className={'FilterResetButton'} onClick={() => this.resetFilters()}>
                                {t('doctor.filter.ResetChanges')}
                            </div>
                            <div className={'FilterResetButton'} onClick={this.closeSideBar.bind(this)}>
                                {t('doctor.filter.SaveChanges')}
                            </div>
                        </div>

                    </div>
{/*display block to display none*/}

                </div>

                {/*<div className={'register-form-nav-container block'} >*/}
                {/*    <div className={'register-form-nav'}  style={{width:width}}>*/}
                {/*        <label type={"button"} onClick={this.closeSideBar.bind(this)} style={{display:display}} className={"v-centered close-bar"}>Close</label>*/}
                {/*    </div>*/}
                {/*</div>*/}

            </div>
        );
    }
}
// AdminEditForm.propTypes = {
//     isSideBarOpen:Prototypes.bool.isRequired,
//     closeSideBar:Prototypes.func.isRequired,
//     editAdmin:Prototypes.func.isRequired,
//     title: Prototypes.string.isRequired,
//     entry: Prototypes.object
// }

//
function mapDispatchToProps(dispatch) {
    return {
        filterPatientsTable: (type,payload) => dispatch(filterPatientsTable(type,payload))
    };
}
const mapStateToProps = state => {
    return {
        // patientsData:state.patientsTableDataReducer.patientsData,
        // initiatePatientsData:state.patientsTableDataReducer.initiatePatientsData
    };
};

const Filter = compose(
    withTranslation(),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(FilterComposed);

export default Filter;
