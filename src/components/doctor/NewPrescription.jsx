import React from 'react';

import {withTranslation} from "react-i18next";
import {compose} from "redux";
import {connect} from "react-redux";
import {BsX} from 'react-icons/bs';

import '../../components_style/doctorComponents/FormsStyle.css'

class NewPrescriptionComposed extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
        console.log("newPrescriptionComposed")
    }

    componentWillUnmount() {


    }


    async closeSideBar(){
        document.body.style.overflow = "auto"
        this.props.closeSideBar(false)
        this.setState({entry:null})
    }



    render() {

        let display = this.props.sendPhoneClicked ? 'block' : 'none'
        let width = this.props.isPrescriptionOpen ? "94%" : "0px"
        let container_width = this.props.isPrescriptionOpen ? "100%" : "0px"
        let height = this.props.isPrescriptionOpen ? "98%" : "0px"
        let container_height = this.props.isPrescriptionOpen ? "100%" : "0px"
        let {t} = this.props

        return (
            <div className={"sidenav-complete2"} style={{width:container_width, height:container_height}} >
                <div className={"sidenav-complete2"} style={{width:container_width, height:container_height}} onClick={this.closeSideBar.bind(this)}/>
                <div className={"margin-inline sidenav"} style={{width:width, height:height}}>
                    <BsX onClick={this.closeSideBar.bind(this)} size={35} style={{
                        float: 'left',
                        top: '10px',
                        left: '10px',
                        position: 'absolute',
                    }}/>
                    <div className={"existingPatientMargins"}>
                        <div className={'formFont'}> New Prescription</div>

                        <div className={'newPrescriptionBoxes1'}>
                            <div className={'formTitleFont'}> Patient Details</div>
                            <div>
                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Patient Full Name*</label>
                                    <input type="text"  name="name"/>
                                </div>

                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>DOB*</label>
                                    <input type="text"  name="DOB"/>
                                </div>

                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Id*</label>
                                    <input type="text"  name="ID"/>
                                </div>

                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Phone Number</label>
                                    <input type="number"  name="Phone"/>
                                </div>
                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Age</label>
                                    <input type="number"  name="Age"/>
                                </div>
                            </div>
                        </div>

                        <div className={'newPrescriptionBoxes2'}>
                            <div className={'formTitleFont'}> Treatment Details</div>
                            <div>
                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Treatment Duration (min)</label>
                                    <input type="text"  name="minDuration"/>
                                </div>

                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Intensity level will not be greater than</label>
                                    <input type="text"  name="intensity"/>
                                </div>

                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Preventive Treatment Days </label>
                                    <input type="text"  name="preventive"/>
                                </div>
                            </div>
                            <div className={'signingBigSlotsWrapper'}>
                                <div className={'signingBigSlots'}>
                                    <div className={'signingSlots'}> Signature </div>
                                    <div className={'signingSlots'}> Date </div>
                                </div>
                            </div>
                        </div>

                        <div className={'newPrescriptionBoxes3'}>
                            <div className={'formTitleFont'}> Doctor Details</div>
                            <div>
                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Doctor Full Name</label>
                                    <input type="text"  name="doctorName"/>
                                </div>

                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Medical License Number</label>
                                    <input type="number"  name="licenseNum"/>
                                </div>

                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Clinic Address</label>
                                    <input type="text"  name="clinicAdd"/>
                                </div>

                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>City</label>
                                    <input type="text"  name="City"/>
                                </div>
                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>State</label>
                                    <input type="text"  name="State"/>
                                </div>
                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>ZIP Code</label>
                                    <input type="number"  name="ZIP"/>
                                </div>
                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Phone Number</label>
                                    <input type="number"  name="doctorPhone"/>
                                </div>
                                <div className={'inputSlots'}>
                                    <label className={'labelStyle'}>Fax Number</label>
                                    <input type="number"  name="Fax"/>
                                </div>
                            </div>
                        </div>
                        {/*<div className={'ExistingSendButton'} onClick={this.openExistingPatient}> ok </div>*/}

                        <div className={'formButtonsBox'}>
                            <button className={'formButton1'}> Finish and Print</button>
                            <button className={'formButton2'}> Finish </button>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
const NewPrescription = compose(
    withTranslation(),
    /*connect(
        mapStateToProps,
        mapDispatchToProps
    )*/
)(NewPrescriptionComposed);
export default NewPrescription