import React, {Component} from 'react'
// import {history} from "../../utils/history";
//
// import {connect} from "react-redux";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import Chart from "react-apexcharts";


class HealthInformationComposed /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            SleepingHoursOptions: {
                chart: {
                    type: 'line' },
                xaxis: {
                    categories: ['Base','','July 2020', 'Aug 2020', 'Sep 2020'] },
                yaxis: {
                    axisBorder: {show: false}, axisTicks: {show: false}, lines: {show: false},
                    labels: {/*show: false,*/ formatter: function (val) {
                            return val;}}}
            },
            SleepingHoursSeries: [{
                name: "series-1",
                data: [5,0, 7, 7, 8] }
            ],

            NumOfExerciseOptions: {
                chart: {
                    type: 'line' },
                xaxis: {
                    categories: ['Base','','July 2020', 'Aug 2020', 'Sep 2020'] },
                yaxis: {
                    axisBorder: {show: false}, axisTicks: {show: false}, lines: {show: false},
                    labels: {/*show: false,*/ formatter: function (val) {
                            return val;}}}
            },
            NumOfExerciseSeries: [{
                name: "series-1",
                data: [2,0, 2, 1,3] }
            ],
        }
    }

    componentDidMount() {
        // this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }

    render() {
        let {data} = this.props
        return (
            <div className={'StatisticsTabMainBox'}>
                <div className={'patientStatisticsTabOuterBox'}>
                    <div className={'patientStatisticsTabInnerBox'}>
                        <div className={'patientStatisticsTitle'}> Sleeping Hours </div>
                        <div className={'patientStatisticsGraphBox'}>
                            <Chart options={this.state.SleepingHoursOptions} series={this.state.SleepingHoursSeries} type="bar" width={400} height={250}/>
                        </div>
                    </div>
                    <div className={'patientStatisticsTabInnerBoxRight'}>
                        <div className={'patientStatisticsTitle'}> Average Number of Exercise Per Week </div>
                        <div className={'patientStatisticsGraphBox'}>
                            <Chart options={this.state.NumOfExerciseOptions} series={this.state.NumOfExerciseSeries} type="bar" width={400} height={250}/>
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}
const HealthInformation = compose(
    withTranslation(),
    /* connect(
         mapStateToProps,
         mapDispatchToProps
     )*/
)(HealthInformationComposed);

export default HealthInformation;
