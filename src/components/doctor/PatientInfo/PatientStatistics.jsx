import React, {Component} from 'react'
// import {history} from "../../utils/history";
//
// import {connect} from "react-redux";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import Chart from "react-apexcharts";


class PatientStatisticsComposed /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            HeadacheDaysOptions: {
                chart: {
                    type: 'line' },
          /*      colors: ['#546E7A','blue',  'blue', '#0575e6', '#0575e6'],
                dataLabels: {
                    enabled: true,
                    // textAnchor: 'start',
                    // style: {
                    //     colors: ['#fff']
                    },*/
                xaxis: {
                    categories: ['Base','', 'Aug', 'Sep', 'Oct'] },
                yaxis: {
                    axisBorder: {show: false}, axisTicks: {show: false}, lines: {show: false},
                    labels: {/*show: false,*/ formatter: function (val) {
                        return val;}}}
            },
            HeadacheDaysSeries: [{
                    name: "series-1",
                    data: [25,0, 25, 18, 12] }
            ],

            HeadacheReliefRateOptions: {
                chart: {
                    type: 'line' },
                xaxis: {
                    categories: [ 'July 2020', 'Aug 2020', 'Sep 2020'] },
                yaxis: {
                    axisBorder: {show: false}, axisTicks: {show: false}, lines: {show: false}, max: 100,
                    labels: {/*show: false,*/ formatter: function (val) {
                            return val;}}}
            },
            HeadacheReliefRateSeries: [{
                name: "series-1",
                data: [70,60, 100] }
            ],

            MedicationDaysOptions: {
                chart: {
                    type: 'line' },
                xaxis: {
                    categories: ['Base','', 'Aug', 'Sep', 'Oct'] },
                yaxis: {
                    axisBorder: {show: false}, axisTicks: {show: false}, lines: {show: false},
                    labels: {/*show: false,*/ formatter: function (val) {
                            return val;}}}
            },
            MedicationDaysSeries: [{
                name: "series-1",
                data: [25,0, 13, 9, 12] }
            ],

            AveragePainIntensityOptions: {
                chart: {
                    type: 'line' },
                xaxis: {
                    categories: ['July 2020', 'Aug 2020', 'Sep 2020'] },
                yaxis: {
                    axisBorder: {show: false}, axisTicks: {show: false}, lines: {show: false},
                    labels: {/*show: false,*/ formatter: function (val) {
                            return val;}}}
            },
            AveragePainIntensitySeries: [{
                name: "series-1",
                data: [5, 3, 5] }
            ],

            TriggersOptions: {
                chart: {
                    type: 'line' },
                xaxis: {
                    categories: ['July 2020', 'Aug 2020', 'Sep 2020'] },
                yaxis: {
                    yaxisBorder: {show: false}, yaxisTicks: {show: false}, lines: {show: false},
                    labels: {/*show: false,*/ formatter: function (val) {
                            return val;}}}
            },
            TriggersSeries: [{
                name: "Caffeine",
                data: [5, 3, 5] },{
                name: "Alcohol",
                data: [6, 1, 4] },{
                name: "Stress",
                data: [5, 1, 3] },{

            }
            ],

            // HeadacheDaysOptions: {
            //     chart: {
            //         type: 'line' },
            //     xaxis: {
            //         categories: ['Base','', 'Aug', 'Sep', 'Oct'] },
            //     yaxis: {
            //         axisBorder: {show: false}, axisTicks: {show: false}, lines: {show: true},
            //         labels: {show: false, formatter: function (val) {
            //                 return val;}}}
            // },
            // HeadacheDaysSeries: [{
            //     name: "series-1",
            //     data: [25,0, 13, 9, 12] }
            // ],

        }
    }

    componentDidMount() {
        // this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }

    render() {
        let {data} = this.props
        return (
            <div className={'StatisticsTabMainBox'}>
                <div className={'patientStatisticsTabOuterBox'}>
                   <div className={'patientStatisticsTabInnerBox'}>
                       <div className={'patientStatisticsTitle'}> Headache Days </div>
                       <div className={'patientStatisticsGraphBox'}>
                           <Chart options={this.state.HeadacheDaysOptions} series={this.state.HeadacheDaysSeries} type="bar" width={400} height={250}/>
                       </div>
                   </div>
                    <div className={'patientStatisticsTabInnerBoxRight'}>
                        <div className={'patientStatisticsTitle'}> Headache Relief Rate (%) </div>
                        <div className={'patientStatisticsGraphBox'}>
                            <Chart options={this.state.HeadacheReliefRateOptions} series={this.state.HeadacheReliefRateSeries} type="bar" width={400} height={250}/>
                        </div>
                    </div>
                </div>
                <div className={'patientStatisticsTabOuterBox'}>
                    <div className={'patientStatisticsTabInnerBox'}>
                        <div className={'patientStatisticsTitle'}> Medication Days </div>
                        <div className={'patientStatisticsGraphBox'}>
                            <Chart options={this.state.MedicationDaysOptions} series={this.state.MedicationDaysSeries} type="bar" width={400} height={250}/>
                        </div>
                    </div>
                    <div className={'patientStatisticsTabInnerBoxRight'}>
                        <div className={'patientStatisticsTitle'}> Average Pain Intensity </div>
                        <div className={'patientStatisticsGraphBox'}>
                            <Chart options={this.state.AveragePainIntensityOptions} series={this.state.AveragePainIntensitySeries} type="bar" width={400} height={250}/>
                        </div>
                    </div>
                </div>
                <div className={'patientStatisticsTabOuterBox'}>
                    <div className={'patientStatisticsTabInnerBox'}>
                        <div className={'patientStatisticsTitle'}> Triggers </div>
                        <div className={'patientStatisticsGraphBox'}>
                            <Chart options={this.state.TriggersOptions} series={this.state.TriggersSeries} type="bar" width={400} height={250}/>
                        </div>
                    </div>
                    <div className={'patientStatisticsTabInnerBoxRight'}>
                        <div className={'patientStatisticsTitle'}> Pain Location </div>
                       {/* <div className={'patientStatisticsGraphBox'}>
                            <Chart options={this.state.options2} series={this.state.series2} type="bar" width={200} height={200}/>
                        </div>*/}
                    </div>
                </div>
            </div>
        )

    }
}
const PatientStatistics = compose(
    withTranslation(),
    /* connect(
         mapStateToProps,
         mapDispatchToProps
     )*/
)(PatientStatisticsComposed);

export default PatientStatistics;
