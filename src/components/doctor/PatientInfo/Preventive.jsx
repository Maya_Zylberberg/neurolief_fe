import React, {Component} from 'react'
// import {history} from "../../utils/history";
//
// import {connect} from "react-redux";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import Chart from "react-apexcharts";
import {CircularProgressbarWithChildren} from "react-circular-progressbar";


class PreventiveComposed /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            options1: {
                chart: {
                    type: 'line'
                    // id: "basic-bar"
                },
                xaxis: {
                    categories: ['Base','', 'Aug', 'Sep', 'Oct']
                },
                yaxis: {
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false,
                    },
                    lines: {
                        show: true
                    },
                    labels: {
                        show: false,
                        formatter: function (val) {
                            return val;
                        }
                    }
                }

            },
            series1: [
                {
                    name: "series-1",
                    data: [25,0, 13, 9, 12]
                }
            ],

            options2: {
                chart: {
                    type: 'line'
                    // id: "basic-bar"
                },
                xaxis: {
                    categories: [ 'Aug', 'Sep', 'Oct']
                },
                yaxis: {
                    axisBorder: {
                        show: false
                    },
                    axisTicks: {
                        show: false,
                    },
                    lines: {
                        show: true
                    },
                    labels: {
                        show: false,
                        formatter: function (val) {
                            return val;
                        }
                    }
                }

            },
            series2: [
                {
                    name: "series-2",
                    data: [85,60,80]
                }
            ]
        };
    }

    componentDidMount() {
        // this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }

    render() {
        let {data} = this.props
        return (
            <div className={'abortiveMainBox'}>
                <div className={'abortiveInsideLeftBoxes'}>
                    <div className={'abortiveBoxesTitle'}> Headache Days</div>
                    <div className={'chartBox'}>
                        <Chart options={this.state.options1} series={this.state.series1} type="bar" width={240} height={200} />
                    </div>
                    <div className={'chartFontNeg'}> 45% more then last month</div>

                </div>
                <div className={'abortiveInsideLeftBoxes'}>
                    <div className={'abortiveBoxesTitle'}>Headache Relief Success Rate</div>
                    <div className={'chartBox'}>
                        <Chart options={this.state.options2} series={this.state.series2} type="bar" width={200} height={200} />

                    </div>
                    <div className={'chartFontPos'}> 30% more then last month </div>


                </div>
                <div className={'abortiveInsideRightBox'}>

                    <div className={'abortiveInsideRightSmallFirstBox'}>
                        <div className={'abortiveBoxesTitle'}> Average Treatment Time</div>
                        <div className={'ProgressbarBox'}>
                            <CircularProgressbarWithChildren value={75} >
                                {/* Put any JSX content in here that you'd like. It'll be vertically and horizonally centered. */}
                                <div style={{ fontSize: 26}}> <strong>20</strong> min </div>
                                <div style={{ fontSize: 20, marginTop: -5 }}>
                                    /30 min
                                </div>
                            </CircularProgressbarWithChildren>
                        </div>
                        <div className={'ProgressbarFont'}> Change Regimen</div>
                    </div>

                    <div className={'abortiveDivider'}/>

                    <div className={'abortiveInsideRightSmallSecondBox'}>
                        <div className={'abortiveBoxesTitle'}> Average Intensity</div>
                        <div className={'ProgressbarBox'}>
                            <CircularProgressbarWithChildren value={90} >
                                {/* Put any JSX content in here that you'd like. It'll be vertically and horizonally centered. */}
                                <div style={{ fontSize: 26}}><strong>18</strong> min </div>
                                <div style={{ fontSize: 20, marginTop: -5 }}>
                                    /20 min
                                </div>
                            </CircularProgressbarWithChildren>
                        </div>
                        <div className={'ProgressbarFont'}> Change Regimen</div>

                    </div>
                </div>

            </div>
        )

    }
}
const Preventive = compose(
    withTranslation(),
    /* connect(
         mapStateToProps,
         mapDispatchToProps
     )*/
)(PreventiveComposed);

export default Preventive;
