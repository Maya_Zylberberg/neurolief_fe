import React, {Component} from 'react'
// import {history} from "../../utils/history";
//
// import {connect} from "react-redux";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import {RiEditLine} from 'react-icons/ri';



class TreatmentRegimenComposed /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        // this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }


    render() {
        let {t} = this.props

        return (
            <div>
                <div className={'treatmentRegimenBoxesBox'}>
                    <div className={`treatmentRegimenBox1`} >
                        {t('doctor.patientInfo.Abortive')}
                        <div className={'treatmentRegimenNumber'}>60</div>
                        <div className={'treatmentRegimenValue'}> {t('doctor.patientInfo.TreatmentDuration(min)')}  </div>
                    </div>
                    <div className={`treatmentRegimenBox2`}>
                        <div> {t('doctor.patientInfo.Preventive')} </div>
                        <div className={'PreventiveWrapper'}>
                            <div className={'treatmentRegimenNumber'}>20</div>
                            <div className={'treatmentRegimenValue'}> {t('doctor.patientInfo.TreatmentDuration(min)')} </div>
                            <div className={'treatmentRegimenNumber'}>30</div>
                            <div className={'treatmentRegimenValue'}> {t('doctor.patientInfo.MaximalIntensity')} </div>
                        </div>
                        <div className={'PreventiveWrapper'}>
                            <div className={'treatmentRegimenNumber'}>Sun, Wed, Fri</div>
                            <div className={'treatmentRegimenValue'}> {t('doctor.patientInfo.Frequency')}  </div>
                            <div className={'treatmentRegimenNumber'}> {t('doctor.patientInfo.Morning')} </div>
                            <div className={'treatmentRegimenValue'}>{t('doctor.patientInfo.TreatmentTime')}   </div>
                        </div>

                    </div>
                    <div className={`treatmentRegimenBox3`}>
                        <div className={'treatmentRegimenBox3TitleWrapper'}>
                            <div className={'treatmentRegimenBox3left'}> {t('doctor.patientInfo.Additional_instructions_for_treatment')}</div>
                            <div className={'treatmentRegimenBox3right'}><RiEditLine/> {t('doctor.patientInfo.edit')}</div>
                        </div>

                        <div className={'treatmentRegimenNumber'}>use the device daily for 20 minutes.</div>
                    </div>
                </div>
            </div>
        )

    }
}
const TreatmentRegimen = compose(
    withTranslation(),
    /* connect(
         mapStateToProps,
         mapDispatchToProps
     )*/
)(TreatmentRegimenComposed);

export default TreatmentRegimen;
