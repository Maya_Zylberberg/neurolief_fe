import React from "react";
import { FaOilCan } from 'react-icons/fa';
import { RiPlantLine } from 'react-icons/ri';
import { BsFillDropletFill } from 'react-icons/bs';
import {withTranslation} from "react-i18next";
import {compose} from "redux";


class FlagsComposed extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {
        let {t} = this.props
        let {title} = this.state
        return (
            <div className={'PatientDashboardBox'}>
                <div className="PatientDashboardBox-item active"><BsFillDropletFill size={30}/> 33333{t('doctor.patients')} <span className="sr-only">(current)</span></div>
                <div className="PatientDashboardBox-item active"> <RiPlantLine size={35}/>{t('doctor.patients')}</div>
                <div className="PatientDashboardBox-item active"><BsFillDropletFill size={30}/>+ <RiPlantLine size={35}/> {t('doctor.patients')} </div>
            </div>
        );
    }
}
const Flags = compose(
    withTranslation(),
    /*connect(
        mapStateToProps,
        mapDispatchToProps
    )*/
)(FlagsComposed);

export default Flags;
