import React, {Component} from "react";
import {BsX} from 'react-icons/bs';
import Constants from "../../utils/constants";

import {compose} from "redux";
import {withTranslation} from "react-i18next";
import ExistingPatient from "./ExistingPatient";
import {history} from "../../utils/history";


class AddPatientComposed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isNewPatientOpen:false,
            isExistingPatientOpen:false

        };
    }

    async closeSideBar(){
        document.body.style.overflow = "auto"
        this.props.closeSideBar(false)
        this.setState({entry:null})
    }
    async openNewPatient(){
        document.body.style.overflow = "auto"
        this.props.closeSideBar(false)
        this.setState({entry:null})

        history.push({
            pathname: '/dashboard/Patient',
            state: {
                Email:this.state.username,
                from: {
                    fromPath: `/dashboard`,
                }
            },
        })
    }


    async openExistingPatient(){
        document.body.style.overflow = "auto"
        document.body.style.overflow = "hidden"
        this.setState({isExistingPatientOpen:true})
        /*this.props.closeSideBar(false)
       this.setState({entry:null})*/
    }

    render() {
        let display = this.props.isSideBarOpen ? 'block' : 'none'
        let width = this.props.isSideBarOpen ? "600px" : "0px"
        let container_width = this.props.isSideBarOpen ? "100%" : "0px"
        let {t} = this.props
        let {isNewPatientOpen} = this.state
        let {isExistingPatientOpen} = this.state


        return (
            <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} >
                <div className={"sidenav-complete2"} style={{width:container_width, height:"100%"}} onClick={this.closeSideBar.bind(this)}/>
                <div className={"sidenav"} style={{width:width, height:"100%", float:'right'}}>
                    <BsX onClick={this.closeSideBar.bind(this)} size={35} style={{
                        float: 'left',
                        top: '10px',
                        left: '10px',
                        position: 'absolute',
                    }}/>
                    <div className={'filterFont'}> {t('doctor.addPatient.addPatient')}</div>
                    <div className={'filterBorders'}>
                        <div className={'addPatientBox'} onClick={this.openNewPatient.bind(this)} >
                            <div className={'addPatientBoxTitle'}>  {t('doctor.addPatient.new')}   </div>
                            <div className={'addPatientBoxContent'}>  {t('doctor.addPatient.new_content')}  </div>
                        </div>
                        <div className={'addPatientBox'} onClick={this.openExistingPatient.bind(this)} >
                            <div className={'addPatientBoxTitle'}>  {t('doctor.addPatient.existing')}   </div>
                            <div className={'addPatientBoxContent'}>  {t('doctor.addPatient.existing_content')}  </div>
                        </div>

                    </div>
                    {/*display block to display none*/}

                </div>
    {/*            {isNewPatientOpen && <TabsDashboard
                    title={'Edit Admin'}
                    isNewPatientOpen={isNewPatientOpen}
                    closeSideBar={() => this.setState({isSideBarOpen: false})}/>}*/}

                {isExistingPatientOpen && <ExistingPatient
                    title={'Edit Admin'}
                    isExistingPatientOpen={isExistingPatientOpen}
                    closeSideBar={() => this.setState({isExistingPatientOpen: false})}/>}
            </div>
        );
    }
}


const AddPatient = compose(
    withTranslation(),
    /*connect(
        mapStateToProps,
        mapDispatchToProps
    )*/
)(AddPatientComposed);

export default AddPatient;
