import React, {Component} from 'react'
import PropTypes from "prop-types";
import utils from "../../utils/utils";
import Error from "../single_components/Error";
import Constants from "../../utils/constants";
import NovaSelect from "../single_components/NovaSelect";
import PhoneInput from "react-phone-input-2";

class Eyecare_provider extends Component {

    constructor(props) {
        super(props);
        this.MedicalLicense = {
            entry: this.props.entry,
        }
    }




    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.entry !== prevProps.entry) {
            this.setState({
                entry: this.props.entry
            })
        }
    }


    render() {
        return (
            <div className={'Patient-personal-Form'}>
                <div className={'Patient-personal-form-container'}>
                    <div className={'Patient-personal-form-element'}>
                        <label className={'patient-form-label nova-form-label'}>First name *</label>
                        <div className={'form-search'}>
                            <input
                                type={"text"}
                                disabled={this.props.isDisabled}
                                className={"nova-form-input"}
                                maxLength={20}
                                defaultValue={utils.get(this.props.entry,'FirstName') !== null &&
                                utils.get(this.props.entry,'FirstName') !== undefined ?
                                    utils.get(this.props.entry,'FirstName') :''}
                                name={'FirstName'}
                                onChange={(event) => this.props.onValueChange('FirstName', event.target.value)}
                            />
                            <Error errorMessage={utils.get(this.props.errors,'FirstName')} isShown={!!utils.get(this.props.errors, 'FirstName')}/>
                        </div>
                    </div>
                    <div className={'Patient-personal-form-element'}>
                        <label className={'patient-form-label nova-form-label'}>Last name *</label>
                        <div className={'form-search'}>
                            <input
                                type={"text"}
                                disabled={this.props.isDisabled}
                                className={"nova-form-input"}
                                maxLength={50}
                                defaultValue={utils.get(this.props.entry,'LastName') !== null &&
                                utils.get(this.props.entry,'LastName') !== undefined ?
                                    utils.get(this.props.entry,'LastName') :''}
                                name={'LastName'}
                                onChange={(event) => this.props.onValueChange('LastName', event.target.value)}
                            />
                            <Error errorMessage={utils.get(this.props.errors,'LastName')} isShown={!!utils.get(this.props.errors, 'LastName')}/>
                        </div>
                    </div>
                    <div className={'Patient-personal-form-element'}>
                        <label className={'patient-form-label nova-form-label'}>Email *</label>
                        <div className={'form-search'}>
                            <input
                                type={"text"}
                                disabled={this.props.isDisabled}
                                className={"nova-form-input"}
                                maxLength={100}
                                defaultValue={utils.get(this.props.entry,'Email') !== null &&
                                utils.get(this.props.entry,'Email') !== undefined ?
                                    utils.get(this.props.entry,'Email') :''}
                                name={'Email'}
                                onChange={(event) => this.props.onValueChange('Email', event.target.value)}
                            />
                            <Error errorMessage={utils.get(this.props.errors,'Email')} isShown={!!utils.get(this.props.errors, 'Email')}/>
                        </div>
                    </div>
                </div>
                <div className={'Patient-personal-form-container'}>
                    <div className={'Patient-personal-form-element'}>
                        <label className={'patient-form-label nova-form-label'}>Profession</label>
                        <div className={'form-search'}>
                            <NovaSelect
                                disabled={this.props.isDisabled}
                                name={'Profession'}
                                options={Constants.Profession}
                                className={'nova-form-input nova-form-searchableSelect'}
                                value={Constants.Profession.find(x => x.value === utils.get(this.props.entry,'Profession'))}
                                placeholder={"Select a Profession"}
                                onChange={e => {
                                    this.props.onValueChange('Profession', e.value)
                                }}
                            />
                            <Error errorMessage={utils.get(this.props.errors,'Profession')} isShown={!!utils.get(this.props.errors, 'Profession')}/>
                        </div>
                    </div>
                    <div className={'Patient-personal-form-element'}>
                        <label className={'patient-form-label nova-form-label'}>Phone number *</label>
                        <div className={'form-search'}>
                            <PhoneInput
                                country={'us'}
                                value={utils.get(this.props.entry,'PhoneNumber') !== null &&
                                utils.get(this.props.entry,'PhoneNumber') !== undefined?
                                    utils.get(this.props.entry,'PhoneNumber') :''}
                                inputClass={'nova-form-input'}
                                containerClass={'nova-PhoneInput-container'}
                                buttonClass={'nova-PhoneInput-button'}
                                dropdownClass={`nova-patient-PhoneInput-dropdown`}
                                searchClass={'nova-patient-PhoneInput-search'}
                                enableSearch={true}
                                countryCodeEditable={false}
                                autoFormat={true}
                                onChange={phone =>this.props.onValueChange('PhoneNumber',phone)}
                            />
                            <Error errorMessage={utils.get(this.props.errors,'PhoneNumber')} isShown={!!utils.get(this.props.errors, 'PhoneNumber')}/>
                        </div>
                    </div>
                    <div className={'Patient-personal-form-element'}>
                        <label className={'patient-form-label nova-form-label'}>Medical license</label>
                        <div className={'form-search'}>
                            <input
                                type={"text"}
                                disabled={this.props.isDisabled}
                                className={"nova-form-input"}
                                maxLength={50}
                                defaultValue={utils.get(this.props.entry,'MedicalLicense') !== null &&
                                utils.get(this.props.entry,'MedicalLicense') !== undefined ?
                                    utils.get(this.props.entry,'MedicalLicense') :''}
                                name={'MedicalLicense'}
                                onChange={(event) => this.props.onValueChange('MedicalLicense', event.target.value)}
                            />
                            <Error errorMessage={utils.get(this.props.errors,'MedicalLicense')} isShown={!!utils.get(this.props.errors, 'MedicalLicense')}/>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

Eyecare_provider.propTypes = {
    onValueChange:PropTypes.func.isRequired,
    entry: PropTypes.object.isRequired,
    isDisabled: PropTypes.bool
}

export default Eyecare_provider;
