import React from "react";
import PropTypes from "prop-types";
import {Modal} from 'react-bootstrap'
import {confirmable, createConfirmation} from "react-confirm";

import SvGraphics from "../../assets/SvGraphics";
import {compose} from "redux";
import {withTranslation} from "react-i18next";

class ErrorModalComposed extends React.Component {

    componentDidMount() {
        document.getElementById('err-modal-ok-button').focus();
    }

    render() {
        let {
            proceedLabel,
            title,
            errMessages,
            show,
            proceed,
            enableEscape = true,
            t
        } = this.props;

        return (
                <Modal
                    centered={true}
                    show={show}
                    onHide={() => proceed(false)}
                    backdrop={enableEscape ? true : "static"}
                    keyboard={enableEscape}
                    dialogClassName={'nova-modal'}>

                    <Modal.Body className={'nova-modal-body'}>
                        <SvGraphics onClick={() => proceed(false)}  svgname={'close'} height={'15px'} width={'15px'} style={{
                            float: 'left',
                            top: '10px',
                            left: '10px',
                            position: 'absolute',
                            cursor: 'pointer'
                        }}/>
                            <label className={'nova-modal-title'}>{title}</label>
                            <div className={'nova-modal-content red'}>
                                {
                                    errMessages && errMessages.map(txt => {

                                        return (
                                            <>
                                                <label style={{marginBottom: '1rem'}}>{txt}</label>
                                                <br/>
                                            </>
                                            )
                                        }
                                    )
                                }
                            </div>
                        <div className={'nova-modal-footer h-centered'}>
                            <button id={'err-modal-ok-button'} className={'btn btn-primary modal-confirm'}
                                    onClick={() => proceed(true)} onKeyPress={event => {
                                if (event.key === 'Enter') {
                                    proceed(true)
                                }
                            }}>{proceedLabel === "Accept"?t('modal.accept'):proceedLabel}</button>
                        </div>
                    </Modal.Body>
                </Modal>
        );
    }
}

let ErrorModal = compose(
    withTranslation()
)(ErrorModalComposed)

ErrorModal.propTypes = {
    okLabbel: PropTypes.string,
    title: PropTypes.string,
    errMessages: PropTypes.array,
    show: PropTypes.bool,
    proceed: PropTypes.func, // called when ok button is clicked.
    enableEscape: PropTypes.bool
};

export function makeError({proceedLabel: proceedLabel = "Accept", options: options = {}},errMessages) {
    return createConfirmation(confirmable(ErrorModal))({
        errMessages,
        proceedLabel,
        ...options
    });
}
