import React, {Component} from 'react'
import PropTypes from "prop-types";
import FormElementContainer from "../forms_builder/FormElementContainer";
import PersonalNotes from "../forms_builder/PersonalNotes";
import '../../components_style/PatientFormPage1.css'
import '../../components_style/rtl_css/PatientFormPage1.css'
import Address from "../forms_builder/Address";
import PatientRemoteTesting from "../forms_builder/PatientRemoteTesting";
import PatientDetails from "../forms_builder/PatientDetails";

class PatientFormPage1 extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount = () => {

    }


    onValueChange = (name,value) => {
        this.props.onValueChange(name,value)
    }

    render() {

        return (
                <div className={'nova-patient-form-body container'}>

                    <FormElementContainer title={'Patient Details'}
                                        Component={PatientDetails}
                                        rest={{
                                            onValueChange:(name,value) => this.onValueChange(name,value) ,
                                            entry:this.props.entry,
                                            errors:this.props.errors
                                        }}/>

                    <FormElementContainer title={'Remote Testing'}
                                        Component={PatientRemoteTesting}
                                        rest={{
                                            onValueChange:(name,value) => this.onValueChange(name,value) ,
                                            entry:this.props.entry,
                                            errors:this.props.errors
                                        }}/>

                    <FormElementContainer title={'Address'}
                                        Component={Address}
                                        rest={{
                                            onValueChange:(name,value) => this.onValueChange(name,value) ,
                                            entry:this.props.entry,
                                            errors:this.props.errors
                                        }}/>

                    <FormElementContainer title={'Notes'}
                                        Component={PersonalNotes}
                                        rest={{
                                            onValueChange:(name,value) => this.onValueChange(name,value) ,
                                            entry:this.props.entry,
                                            errors:this.props.errors
                                        }}/>

                </div>
        )
    }
}

PatientFormPage1.propTypes = {
    onValueChange:PropTypes.func.isRequired,
    entry: PropTypes.object.isRequired,
    isDisabled: PropTypes.bool
}

export default PatientFormPage1;
