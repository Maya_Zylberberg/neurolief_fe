import React, {Component} from 'react'
import '../../components_style/GraphNavigator.css'
import SvGraphics from "../../assets/SvGraphics";
class GraphNavigator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sideNaveWith: '0px',
            isSideNaveOpen: false
        }
    }

    componentDidMount = () => {
    }

    setSideNaveStatus() {
        let {isSideNaveOpen} = this.state
        this.setState({
            sideNaveWith: isSideNaveOpen ? '0px' : '360px',
            isSideNaveOpen: !isSideNaveOpen
        })
    }

    render() {
        let {sideNaveWith} = this.state
        return (
            <div className={'graph-navigator-container'}>
                <div className={'graph-controller-container'}>
                    <div className={'h-centered graph-navs-container'}>
                        <div className={'graph-navs'}>
                            <div className={'h-centered nav-button-container'}>
                                <button className={'nav-button'}>Test</button>
                            </div>
                            <div className={'h-centered nav-button-container'}>
                                <button className={'nav-button'}>Test</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={'graphs-container'}>
                    <div className={'graph-container'}>
                        <div className={'graph-controller-side-nav'} style={{width:sideNaveWith}}>

                        </div>
                        <div className={'graph-controller-side-nav-controller'} onClick={event => this.setSideNaveStatus()}>
                            <SvGraphics className={'centered nav-controller'} svgname={'controls'} style={{position:'relative',width:'40px',height:'40px'}}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
GraphNavigator.propTypes = {
}

export default GraphNavigator;
