
import React, {Component} from 'react'
import '../../components_style/PatientScreen.css'
import '../../components_style/rtl_css/PatientScreen.css'
import utils from "../../utils/utils";
import SvGraphics from "../../assets/SvGraphics";
import {doctorApi} from "../../services/ApiService";
import ConstantsUtils from "../../utils/ConstantsUtils";
import Constants from "../../utils/constants";
import {history} from "../../utils/history";
import {sendMessage} from "../modals/MessageModal";
import AlertConfirm from "../modals/Confirm";


class PatientPersonal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            patient: null,
            isPersonalInfoOpen: false,
            isContactInfoOpen: false,
            isNotesOpen: false
        }
    }

    async componentWillMount() {

    }

    componentDidMount = () => {
        let patient = this.props.patient
        this.setState({patient})
    }

    manageInfoExpand = (infoField, target) => {
        let {isPersonalInfoOpen, isContactInfoOpen, isNotesOpen} = this.state
        if (infoField === 'pi') this.setState({isPersonalInfoOpen:!isPersonalInfoOpen})
        else if (infoField === 'ci') this.setState({isContactInfoOpen:!isContactInfoOpen})
        else if (infoField === 'pn') this.setState({isNotesOpen:!isNotesOpen})
    }

    deletePatient = async (patient) => {
        let confirm = await AlertConfirm({
            options:{
                extraMessage:'This action cannot be undone',
                title: `Delete patient`,
                warningRed: true
            }},`Are you sure you want to delete the patient: ${this.props.patientName}?`)
        if (confirm) {
            let response = await doctorApi.deletePatient(patient.UserID)
            if (response) {
                let userId = patient.UserID
                await this.props.deletePatient(userId)
                this.props.onNavBack()
            }
        }
    }

    editPatientDetails = (patientData) => {
        let patient = Object.assign({},patientData)
        Object.entries(patient.PII).forEach(entry => {
            let [key, value] = entry
            patient[key] = value
        })
        delete patient.PII
        history.push({
            pathname: `/dashboard/EditPatient`,
            state: {
                patient,
                from: {
                    fromPath: `/dashboard`,
                }
            },
        })
    }

    sendMessage = async () => {
        let Method = await sendMessage({
        },[{id:this.props.patientId,name:this.props.patientName}]);
        if (Method){
            let PatientsIDs = [this.props.patientId]
            await doctorApi.sendMessageToPatients({Method,PatientsIDs})
        }
    }

    render() {
        let patient = this.props.patient
        let device = patient ? patient.Device : undefined

        let lastVisit = this.props.lastVisit ? this.props.lastVisit : {}
        let AmblyopicEye = lastVisit ?  lastVisit['AmblyopicEye']: undefined

        let glassModel = ''
        if (!!AmblyopicEye) {
            if (AmblyopicEye.toString() === '1') {glassModel = 'B'} else if (AmblyopicEye.toString() === '2') {glassModel = 'A'}
        }
        let accountStatus = patient ? ConstantsUtils.getPatient_AccountStatus(patient.AccountStatus) : undefined
        let lockedStatus = patient ? patient.PII.User.Enabled : 1
        let lockedStatusLabel = 'Locked'
//
        let gender =  patient ? Constants.Genders.find(base => base.value === patient.PII.Gender).label : undefined
        let birthdate =  patient ? utils.getDateFormatWithYear(new Date(patient.PII.Birthdate)): undefined
        let address = patient ?
            <>{`${patient.PII.Apartment} ${patient.PII.Street}`}<br/>{`${patient.PII.State}${patient.PII.State?' ':''}${patient.PII.Country}`}</>
            : undefined

        let PhoneNumber = patient ? patient.PII.PhoneNumber : undefined
        let Email = patient ? patient.PII.Email : undefined

        let notes = patient ? patient.Notes : ''

        let Status = patient ? patient.Status : 0

        return (
            <div className={'nova-patient-left inline'}>
                {Status === 1 && <div className={'nova-patient-screen-Status'}>
                    <div className={'status-icon'}>
                        <SvGraphics svgname={'warning-red'} className={'h-centered'} style={{width: '30px'}}/>
                    </div>
                    <div className={'status-label red '}>
                        <label className={'red h-centered'}>
                            {'Missed more than 30%'}
                        </label>
                    </div>
                    <div className={'status-send-message red h-centered'} onClick={this.sendMessage.bind(this)}>
                        <label className={'red link h-centered'}>
                            Send Message
                        </label>
                    </div>
                </div>}
                <div className={'nova-patient-screen-info'}>
                    <div className={'patient-device-info'}>
                        <div className={'patient-device h-centered'}>
                            <table className={'centered'} style={{width:'90%'}}>
                                <tr className={'patient-personal-tr'}>
                                    <td className={'patient-personal-td-label-left'}><label
                                        className={'patient-personal-td-label'}>Device SN</label></td>
                                    {device && <td className={'patient-personal-td-right'}><label
                                        className={'patient-personal-td-label bolded'}>{device.SerialNumber}</label>
                                    </td>}
                                </tr>
                                <tr className={'patient-personal-tr'}>
                                    <td className={'patient-personal-td-label-left'}><label
                                        className={'patient-personal-td-label'}>Glass Model</label></td>
                                    <td className={'patient-personal-td-right'}><label
                                        className={'patient-personal-td-label bolded'}>{glassModel}</label>
                                    </td>
                                </tr>
                                <tr className={'patient-personal-tr'}>
                                    <td className={'patient-personal-td-label-left'}><label className={'patient-personal-td-label'}>Account Status</label></td>
                                    <td className={'patient-personal-td-right'}><label className={'patient-personal-td-label bolded'}>{accountStatus}</label></td>
                                    {!lockedStatus && <td>
                                        <SvGraphics className={'patient-personal-td-label-lock'}
                                                   svgname={'lock'}
                                                   style={{
                                                       width: '15px',
                                                       height: '15px',
                                                       position: 'relative',
                                                       marginBottom: '6px'
                                                   }}/>
                                            <label className={'patient-personal-td-label'}>
                                            {lockedStatusLabel}
                                            </label></td>}
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div className={'patient-info-options h-centered'}>
                        <div className={'patient-info-options-element patient-personal-information-edit-container'}>
                            <label onClick={this.manageInfoExpand.bind(this,'pi')} className={'patient-personal-label patient-personal-information-edit-label'}>Personal Information</label>
                            <SvGraphics onClick={this.manageInfoExpand.bind(this,'pi')} className={'patient-personal-svg'} svgname={this.state.isPersonalInfoOpen? 'arrow-down' : 'arrow-right'} height={'25px'} width={this.state.isPersonalInfoOpen ? '15px': '9px'}/>
                            {this.state.isPersonalInfoOpen && <div className={'patient-personal-information-view'}>
                                <div className={'patient-personal-information-view-container h-centered'}>
                                    <table className={'centered'} style={{width: '90%'}}>
                                        <tr className={'patient-personal-tr'}>
                                            <td className={'patient-personal-td-left'}><label
                                                className={'patient-personal-td-label'}>Gender</label></td>
                                            <td className={'patient-personal-td-right'}><label
                                                className={'patient-personal-td-label bolded'}>{gender}</label></td>
                                        </tr>
                                        <tr className={'patient-personal-tr'}>
                                            <td className={'patient-personal-td-left'}><label
                                                className={'patient-personal-td-label'}>Birthdate</label></td>
                                            <td className={'patient-personal-td-right'}><label
                                                className={'patient-personal-td-label bolded'}>{birthdate}</label></td>
                                        </tr>
                                        <tr className={'patient-personal-tr'}>
                                            <td className={'patient-personal-td-left'}><label
                                                className={'patient-personal-td-label'}>Address</label></td>
                                            <td className={'patient-personal-td-right'}><label
                                                className={'patient-personal-td-label bolded'}>{address}</label></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>}
                        </div>
                        <div className={'patient-info-options-element patient-contact-information-edit-container'}>
                            <label onClick={this.manageInfoExpand.bind(this,'ci')} className={'patient-personal-label patient-contact-information-edit-label'}>Remote Testing</label>
                            <SvGraphics onClick={this.manageInfoExpand.bind(this,'ci')} className={'patient-personal-svg'} svgname={this.state.isContactInfoOpen? 'arrow-down' : 'arrow-right'} height={'25px'} width={this.state.isContactInfoOpen ? '15px': '9px'}/>
                            {this.state.isContactInfoOpen && <div className={'patient-contact-information-view'}>
                                <div className={'patient-contact-information-view-container h-centered'}>
                                    <table className={'centered'} style={{width: '90%'}}>
                                        <tr className={'patient-personal-tr'}>
                                            <td className={'patient-personal-td-left'}><label
                                                className={'patient-personal-td-label'}>Phone Number</label></td>
                                            <td className={'patient-personal-td-right'}><label
                                                className={'patient-personal-td-label bolded'}>{PhoneNumber}</label>
                                            </td>
                                        </tr>
                                        <tr className={'patient-personal-tr'}>
                                            <td className={'patient-personal-td-left'}><label
                                                className={'patient-personal-td-label'}>Email 1</label></td>
                                            <td className={'patient-personal-td-right'}><label
                                                className={'patient-personal-td-label bolded'}>{Email}</label></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>}
                        </div>
                        <div className={'patient-info-options-element patient-notes-edit-container'}>
                            <label onClick={this.manageInfoExpand.bind(this,'pn')} className={'patient-personal-label patient-notes-edit-label'}>Notes</label>
                            <SvGraphics onClick={this.manageInfoExpand.bind(this,'pn')} className={'patient-personal-svg'} svgname={this.state.isNotesOpen? 'arrow-down' : 'arrow-right'} height={'25px'} width={this.state.isNotesOpen ? '15px': '9px'}/>
                            {this.state.isNotesOpen && <div className={'patient-notes-view'}>
                                <div className={'patient-notes-view-container patient-personal-td-label h-centered'}>
                                    {notes}
                                </div>
                            </div>}
                        </div>
                    </div>
                    <div className={'patient-options h-centered'}>
                        <button disabled={this.props.completed} onClick={() => this.editPatientDetails(patient)} className={'patient-options-btn patient-options-btn-update btn btn-primary patient-visit-date-button'}>Update Patient Information</button>
                        <button onClick={() => this.deletePatient(patient)} className={'patient-options-btn patient-options-btn-delete btn btn-primary patient-visit-date-button'}>Delete Patient</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default PatientPersonal;

