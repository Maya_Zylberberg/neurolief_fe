import React, {Component} from 'react'
import SvGraphics from "../../assets/SvGraphics";
import utils from "../../utils/utils";
import {history} from "../../utils/history";


class PatientProfileVisitHistory extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    seeVisitReport = (visit) => {
        history.push({
            pathname: `/dashboard/visit-report`,
            state: {
                visits: {Visits:[visit]},
                FullName: this.props.FullName,
                from: {
                    fromPath: `/dashboard/Patient`,
                }
            },
        })
    }

    addNewVisit = async () => {
        if (this.props.lastVisit){
            history.push({
                pathname: `/dashboard/NewVisit`,
                state: {
                    visits: {Visits:[this.props.lastVisit]},
                    FullName: this.props.FullName,
                    from: {
                        fromPath: `/dashboard/Patient`,
                    }
                },
            })
        }
    }

    render() {
        let visits = this.props.visits ? this.props.visits : []

        return (
            <div className={'patient-profile-visit-history-container'}>
                <label className={'table-Navigation-title'}>Visits History</label>
                <div className={'patient-profile-visit-history-table'}>
                    <table className={'patient-profile-table'}>
                        <thead className={'patient-profile-table-header-container'}>
                        <tr className={'patient-profile-table-header'}>
                            <th className={'patient-profile-table-header-label'}>Date</th>
                            <th className={'patient-profile-table-header-label'}
                                colSpan="3">Distance VA (LogMAR)</th>
                            <th className={'patient-profile-table-header-label'}
                                colSpan="2">Stereo Acuity (arcsec)</th>
                        </tr>
                        <tr className={'patient-profile-table-subheader'}>
                            <th/>
                            <th className={'patient-profile-table-subheader-label'}>Right</th>
                            <th className={'patient-profile-table-subheader-label'}>Left</th>
                            <th className={'patient-profile-table-subheader-label'}>Binocular</th>
                            <th/>
                            <th/>
                        </tr>
                        </thead>
                        <tbody className={'patient-profile-table-body-container'}>
                        {
                            visits.map(visit => (
                                <tr className={'patient-profile-table-body-element'}>
                                    <td className={'patient-profile-table-body-label'}>
                                        {utils.getDateFormatWithYear(new Date(visit.VisitDate))}
                                    </td>
                                    <td className={'patient-profile-table-body-label small-cell'}>{visit.RightEyeDistanceVA}</td>
                                    <td className={'patient-profile-table-body-label small-cell'}>{visit.LeftEyeDistanceVA}</td>
                                    <td className={'patient-profile-table-body-label'}>{visit.BinocularEyeFarVA}</td>
                                    <td className={'patient-profile-table-body-label'}>{visit.StereoValue}</td>
                                    <td className={'patient-profile-table-body-label'}>
                                        <text className={'patient-profile-visit-history-link'} onClick={() => this.seeVisitReport(visit)}>To full visit report ></text>
                                    </td>
                                </tr>
                            ))
                        }
                        </tbody>
                    </table>
                </div>
                <button disabled={this.props.completed} className={'visit-history-new-visit-btn btn btn-primary'} onClick={this.addNewVisit.bind(this)}>
                    <SvGraphics className={'patient-timeline-btn-svg'} svgname={'advanced-closed-unfilled'} width={'25px'} height={'25px'}/>
                    <span className={'patient-timeline-btn-label'}>New Visit</span>

                </button>
            </div>
        )
    }
}

export default PatientProfileVisitHistory;
