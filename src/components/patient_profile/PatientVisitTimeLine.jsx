import React, {Component} from 'react'
import '../../components_style/PatientScreen.css'
import '../../components_style/rtl_css/PatientScreen.css'
import utils from "../../utils/utils";
import SvGraphics from "../../assets/SvGraphics";
import {doctorApi} from "../../services/ApiService";
import {history} from "../../utils/history";
import MakeDate from "../modals/DateModal";


class PatientVisitTimeLine extends Component {

    constructor(props) {
        super(props);
        this.state = {
            NextVisit: this.props.NextVisit
        }
    }

    exitButton = () => {
        document.activeElement.blur();
    }
    handleAddDateClicked = async () => {
        let date = await MakeDate({
            options:{

            }
        });
        if (date){
            let response = await doctorApi.editPatient({NextVisit:date},this.props.patientId)
            if (response){
                await this.setState({NextVisit:utils.getDateFormatWithYear(new Date(date))})
                this.props.onNextVisitChange(date)
            }
        }

    }

    addNewVisit = async () => {
        if (this.props.lastVisit){
            history.push({
                pathname: `/dashboard/NewVisit`,
                state: {
                    visits: {Visits:[this.props.lastVisit]},
                    FullName: this.props.FullName,
                    from: {
                        fromPath: `/dashboard/Patient`,
                    }
                },
            })
        }
    }

    seeVisitReport = async () => {
        if (this.props.lastVisit){
            history.push({
                pathname: `/dashboard/visit-report`,
                state: {
                    visits: {Visits:[this.props.lastVisit]},
                    FullName: this.props.FullName,
                    from: {
                        fromPath: `/dashboard/Patient`,
                    }
                },
            })
        }
    }

    render() {
        let lastVisit = this.props.lastVisit ? this.props.lastVisit : {}
        let PreviousDate = !!lastVisit && !!lastVisit['VisitDate'] ?  utils.getDateFormatWithYear(new Date(lastVisit['VisitDate'])) : undefined
        let NextVisit;
        if (!!lastVisit && !!lastVisit['NextVisit'])
            NextVisit = utils.getDateFormatWithYear(new Date(lastVisit['NextVisit']))
        else NextVisit = (<>To Be<br/>Scheduled</>)
        return (
            <div className={'nova-patient-right-right inline'}>
                <div className={'nova-patient-right-right-element nova-patient-right-right-left'}>
                    <div className={'nova-patient-right-right-left-element'} id={'nova-patient-right-right-left-element'}>
                        <label className={'patient-previous-visit-label patient-page-value-label'}>
                            PreviousVisit
                        </label>
                        <label className={'patient-previous-visit-value patient-value'}>
                            {PreviousDate}
                        </label>
                        <button onClick={this.seeVisitReport.bind(this)} className={'patient-previous-visit-report btn btn-primary h-centered patient-visit-date-button'}>See visit report</button>
                    </div>
                </div>
                <div className={'nova-patient-right-right-element nova-patient-right-right-middle'}>
                    <div className={'nova-patient-right-right-left-element'}>
                        <label className={'patient-Next-visit-label patient-page-value-label'}>
                            Next Visit
                        </label>
                        <label className={'patient-Next-visit-value patient-value'}>
                            {NextVisit}
                        </label>

                        <button
                            id={'PatientAddAVisitDate'}
                            disabled={this.props.completed}
                            className={'patient-Next-visit-btn btn btn-primary h-centered patient-visit-date-button'}
                                onClick={e => {
                                    this.handleAddDateClicked(e)
                                    this.exitButton(e)
                                }}>
                            Add a date
                        </button>
                    </div>
                </div>
                <div className={'nova-patient-right-right-element nova-patient-right-right-right'}>
                    <button disabled={this.props.completed} className={'add-new-visit-btn btn btn-primary'} onClick={this.addNewVisit.bind(this)}>
                        <SvGraphics className={'patient-timeline-btn-svg'} svgname={'advanced-closed-unfilled'} width={'25px'} height={'25px'}/>
                        <span className={'patient-timeline-btn-label'}>New Visit</span>

                    </button>
                    <div id={'filler'}/>
                    <button className={'visit-history-btn btn btn-primary'}>
                        <SvGraphics className={'patient-timeline-btn-svg'} svgname={'old'} width={'25px'} height={'25px'}/>
                        <span className={'patient-timeline-btn-label'}>History</span>
                    </button>
                </div>
            </div>
        )
    }
}

export default PatientVisitTimeLine;
