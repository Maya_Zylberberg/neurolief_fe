import React, {Component} from 'react'
import SvGraphics from "../../assets/SvGraphics";
import '../../components_style/BackButton.css'
import '../../components_style/rtl_css/BackButton.css'
import PropTypes from "prop-types";

class BackButton extends Component {

    constructor(props) {
        super(props);

    }

    componentDidMount = ()  => {

    }

    onClick = () => {
       this.props.onBackClick()
    }

    render() {
        return (
            <button className={'back-button-container v-centered'} onClick={this.onClick.bind(this)}>
                <SvGraphics className={'inline back-arrow back-path'} svgname={'back'} height={'15px'}/>
                <label className={'inline back-label'}>{this.props.label?this.props.label:'Back'}</label>
            </button>
        )
    }
}

BackButton.propTypes = {
    onBackClick: PropTypes.func.isRequired,
    label: PropTypes.string
}

export default BackButton;
