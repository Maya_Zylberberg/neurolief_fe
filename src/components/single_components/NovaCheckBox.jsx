import React, {Component} from 'react'
import SvGraphics from "../../assets/SvGraphics";
import 'components_style/NovaCheckBox.css'
import '../../components_style/rtl_css/NovaCheckBox.css'
import PropTypes from "prop-types";

class NovaCheckBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            disabled: false,
            checked: this.props.checked
        };
    }

    componentDidMount = () => {
        if (this.props.checked) {
            this.setState({ checked: this.props.checked });
        }
        if (this.props.disabled) {
            this.setState({ checked: this.props.disabled });
        }
    };

    onCheckboxChange = (id) => {
        if (!this.state.disabled) {
            this.setState({ checked: !this.state.checked });
        }
        this.props.onChange(id);
    };

    render() {
        let checked = this.state.checked;
        let disabled = this.state.disabled;
        let isDisabled = disabled ? "-disabled" : "";
        let isChecked = checked ? "-checked" : "";
        let uniqueId = this.props.rowId
        return (
            <div>
                <input
                    type={"checkbox"}
                    className={"nova-checkbox"}
                    name={`row-${uniqueId}`}
                    id={`row-${uniqueId}`}
                    disabled={disabled}
                    checked={checked}
                    onChange={() => this.onCheckboxChange(uniqueId)}
                />
                <label
                    className={`nova-checkbox-label`}
                    htmlFor={`row-${uniqueId}`}
                    // onClick={e => this.onSelect(`row-${uniqueId}`, this.props.data)}
                >
                    <SvGraphics
                        className={"checkbox-svg"}
                        htmlFor={"this.props.data"}
                        svgname={`checkbox${isDisabled}${isChecked}`}
                        style={{ width: "18px", height: "18px" }}
                    />
                </label>
            </div>
        )
    }
}

NovaCheckBox.propTypes = {
    rowId: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    checked: PropTypes.bool,
};

export default NovaCheckBox
