import React, {Component} from 'react'
import {Navbar,NavDropdown} from "react-bootstrap";
import SvGraphics from "../../assets/SvGraphics";
import ReactFlagsSelect from 'react-flags-select';
import 'react-flags-select/css/react-flags-select.css';
import '../../components_style/NovaNavbar.css'
import '../../components_style/rtl_css/NovaNavbar.css'
import PropTypes from "prop-types";
import Translate from "../../utils/Translate";
import {authenticationService} from "../../services/AuthenticationService";
import {history} from "../../utils/history";
import Faqs from "./Faqs";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import Relivion from "../../assets/Relivion.png";

class NovaNavbarComposed extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentUser: null
        }
    }

    componentDidMount = () /*: void */=> {
        if (this.props && this.props.currentUser) {
            this.setState({currentUser: this.props.currentUser})
        }
    }

    async onSelectFlag(e){
        let lang = Translate.getLanguage(e);
        let language = await Translate.setLanguage(lang);
        const { i18n } = this.props;
        await i18n.changeLanguage(language);
        if (Translate.rtl.includes(language)) document.getElementsByTagName("html")[0].setAttribute("dir","rtl")
        else document.getElementsByTagName("html")[0].setAttribute("dir","ltr")
    }

    handleLogout = async () /*: void*/ => {
        authenticationService.logout();
        history.push('/login')
    };
//to find main dashboard
    render() {
        let {currentUser} = this.state;
        let {t} = this.props
        let currentUsername;
        if (currentUser) {
            currentUsername = currentUser.profile.FirstName + ' ' + currentUser.profile.LastName
        }
        /*mainDashboard*/
        return (
                <Navbar className={'nova-navbar  container'} variant={"dark"}>
                    <img src={Relivion} style={{width:'190px'}}/>

                    {/*<Navbar.Brand><SvGraphics svgname={'eye_swift_blue'} width={'190px'}/></Navbar.Brand>*/}
                    {/*<h1>Logo</h1>*/}
                    <Navbar.Collapse className={"justify-content-end"}>
                        <div className={'nova-left-navbar'}>
                            <div className={'nova-left-navbar-content'}><SvGraphics svgname={'setting-gear'} width={'20px'} height={'20px'} color={'white'} /></div>
                            <div className={'nova-left-navbar-content'}><Faqs/></div>
                        </div>
                        <div className={'nova-right-navbar'}>
                            <ReactFlagsSelect className={'nova-right-navbar-content'}
                                              defaultCountry={Translate.getCurrentCountry()} countries={Translate.getCountries()} showSelectedLabel={false}
                                              showOptionLabel={false} selectedSize={14} onSelect={e => this.onSelectFlag(e)}/>
                        </div>
                        <div className={'nova-middle-navbar'}>
                            <NavDropdown title={
                                <div className={'nova-middle-navbar-dropdown-title'}>
                                    <div className={'nova-middle-navbar-dropdown-title-content'}>
                                        <SvGraphics className={'nova-navbar-singedin-user'} svgname={'person'} width={'20px'} height={'20px'}/>
                                        <label className={'nova-navbar-singedin-user'}>{`${t('defaults.navbar.hello')} ${currentUsername}`}</label>
                                    </div>
                                </div>
                            } id="basic-nav-dropdown">
                                <NavDropdown.Item onClick={this.handleLogout.bind(this)}>{t('defaults.navbar.logout')}</NavDropdown.Item>
                                {/*<NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>*/}
                                {/*<NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>*/}
                            </NavDropdown>
                        </div>

                        {/*<Navbar.Brand href="https://nova-sight.com/"><SvGraphics svgname={'nova_sight'} width={'130px'}/></Navbar.Brand>*/}
                    </Navbar.Collapse>
                </Navbar>
        )
    }
}


let NovaNavbar = compose(
    withTranslation()
)(NovaNavbarComposed)

NovaNavbar.propTypes = {
    currentUser: PropTypes.object
};

export default NovaNavbar;
