import React, {Component} from 'react'
import SvGraphics from "../../assets/SvGraphics";
import Prototypes from 'prop-types'
class Warning extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount = () => {
        if (this.props.tab_option){
            this.setState({tab_option:this.props.tab_option})
        }
    }


    render() {
        return (
            <div className={'warning-message-label'} style={{display: this.props.isShown?'flex':'none', float:this.props.isNonFloat?'none':'left'}}>
                <SvGraphics svgname={'warning'} style={{height:'13px',width:'13px'}} className={'inline v-centered warning-svg'}/>
                <label className={`nova-tab-label yellow inline warning-label v-centered`}>{`${this.props.warningMessage}`}</label>
            </div>
            // call: <Error errorMessage={'Email is required'} isShown={false}/>
        )
    }
}
Warning.Prototypes = {
    warningMessage: Prototypes.string.isRequired,
    isShown: Prototypes.bool.isRequired
};
export default Warning;
