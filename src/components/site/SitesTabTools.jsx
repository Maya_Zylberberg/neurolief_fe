import React, {Component} from "react";
import SvGraphics from "../../assets/SvGraphics";
import Prototypes from 'prop-types'
import SiteEntryForm from "./SiteEntryForm";

class SitesTabTools extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterSiteTable:'',
            searchValue: '',
            isSideBarOpen: false,
            ECPForm:[]
        };
    }

    componentDidMount() {
    }

    filterSiteTable(val){
        this.setState({searchValue:val})
        this.props.filterSiteTable(val)
    }

    openSideBar(){
        document.body.style.overflow = "hidden"
        this.setState({isSideBarOpen:true})
    }

    render() {
        let {t} = this.props
        let {isSideBarOpen} = this.state
        let TabTools_Size = this.props.isSmall ? 'EyeSwiftSuperAdmin-TabTools' : 'Admin-TabTools'
        return (
            <div className={`${TabTools_Size} custom-control`}>
                <div className={"TabTools-container"}>
                    <div className={"rounded-button-wrapper"}>
                        <button disabled type={"button"} className={"btn btn-primary rounded-btn"}>
                            <SvGraphics className={"mail-pic centered"} svgname={'mail'} style={{width: '25px', height: '25px'}}/>
                        </button>
                    </div>
                    {/*<div className={"rounded-button-wrapper"}>
                        <button type={"button"} className={"btn btn-primary rounded-btn"} onClick={e => {
                            let filters = document.getElementsByClassName('table-filter')
                            for (let i = 0; i < filters.length; i++) {
                                if (filters[i].style.display === "none"){
                                    filters[i].style.display = "inline-block";
                                }
                                else filters[i].style.display = "none"
                            }
                        }}>
                            <SvGraphics className={"filter-pic centered"} svgname={'filter'} style={{width: '25px', height: '25px'}}/>
                        </button>
                    </div>*/}
                    <div className={"button-wrapper"}>
                        <button type={"button"} onClick={this.openSideBar.bind(this)} className={"btn btn-primary "}>+ {t('admin.site.tools.add_new')}</button>
                    </div>
                    <div className={"search-wrapper"}>
                        <input className={"nova-input"} placeholder={t('admin.site.tools.search')} type="text" name="search"
                               id={"search"}
                               onChange={event => this.filterSiteTable(event.target.value)} value={this.state.searchValue}/>
                        <SvGraphics className={"input-pic"} svgname={'search'} style={{width: '20px', height: '20px'}}/>
                    </div>
                </div>

                {isSideBarOpen && <SiteEntryForm
                    t={e => this.props.t(e)}
                    title={t('admin.site.tools.add_new')}
                    isSideBarOpen={isSideBarOpen}
                    addNewSite={site => this.props.addNewSite(site)}
                    closeSideBar={() => this.setState({isSideBarOpen: false})}/>}
            </div>
        );
    }
}
SitesTabTools.Prototypes = {
    filterSiteTable: Prototypes.func.isRequired,
}

export default SitesTabTools

