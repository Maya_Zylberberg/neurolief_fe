import {
    PATIENTS_DATA,
    FILTER_PATIENTSDATA,
    PATIENTS_SELECTION,

    PATIENTS_ARCHIVE_DATA,
    FILTER_PATIENTS_ARCHIVEDATA,
    PATIENTS_ARCHIVE_SELECTION
} from "../constants/Doctor-Action-Types";

//PATIENTS-table
export function setPatientsData(payload) {
    return { type: PATIENTS_DATA, payload };
}
export function filterPatientsTable(payload) {
    return { type: FILTER_PATIENTSDATA, payload };
}
export function setPatientsDataSelection(payload) {
    return { type: PATIENTS_SELECTION, payload };
}

//PATIENTS_ARCHIVE-table
export function setPatients_ArchiveData(payload) {
    return { type: PATIENTS_ARCHIVE_DATA, payload };
}
export function filterPatients_ArchiveTable(payload) {
    return { type: FILTER_PATIENTS_ARCHIVEDATA, payload };
}
export function setPatients_ArchiveDataSelection(payload) {
    return {type: PATIENTS_ARCHIVE_SELECTION, payload};
}


