import {
    CLEAN_PATIENT,
    DELETE_PATIENT,
    EDIT_PATIENT,
    FILTER_PATIENTSDATA,
    NEW_PATIENT,
    PATIENT_LOCKED_STATUS,
    PATIENTS_COUNT,
    PATIENTS_DATA,
    PATIENTS_SELECTION,
    PATIENTS_STATUSES
} from "../../constants/Tabs-Action-Types";
import utils from "../../../utils/utils";
import ConstantsUtils from "../../../utils/ConstantsUtils";


const initialState = {
    initiatePatientsData:[],
    patientsData:[],
    filterPatientsData:[],
    selectedPatientsData:[],
    patientsTableFilter:{},
    patientsCount: 0,
    activePatientsCount: 0,
    pendingPatientsCount: 0,
    lockedPatientsCount: 0
};

const getTreatmentDurationLabel = (visit,DateOfActivation) => {
    let TreatmentDuration  =  visit ?  visit['TreatmentDuration']: undefined
    if (DateOfActivation !== null && DateOfActivation !== undefined){
        let total = (TreatmentDuration*30).toString()
        let TreatmentStart = new Date(DateOfActivation);
        let today = new Date();
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const diffDays = Math.round(Math.abs((today - TreatmentStart) / oneDay)).toString();
        return diffDays + '/' + total + ' Days'

    } else {
        let total = (TreatmentDuration*30).toString()
        return '0/' + total + ' Days'
    }

}

const getTreatmentDuration = (visit,DateOfActivation) => {
    let TreatmentDuration  =  visit ?  visit['TreatmentDuration']: undefined
    if (DateOfActivation !== null && DateOfActivation !== undefined){
        let total = TreatmentDuration*30
        let TreatmentStart = new Date(DateOfActivation);
        let today = new Date();
        const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        const diffDays = Math.round(Math.abs((today - TreatmentStart) / oneDay)).toString();
        return ((diffDays/total)*100).toFixed(2)
    } else {
        return 0
    }
}

const patientsTableDataReducer = (state = initialState, action)=> {

    switch(action.type){
        case PATIENTS_COUNT: {
            let _idata = JSON.parse(JSON.stringify(action.payload));
            state = {
                ...state,
                patientsCount: _idata
            };
            break;
        }
        case PATIENTS_DATA: {
            let collection = []

            let active = 0
            let pending = 0
            let locked = 0

            action.payload.forEach(patient => {
                collection.push(
                    {
                        PatientName:patient.PatientName,
                        ID:patient.ID,
                        Sex: patient.Sex,
                        Age:patient.Age,
                        TreatmentStartDate:patient.TreatmentStartDate,
                        TreatmentStartDateVal:patient.TreatmentStartDateVal,
                        LastVisit:patient.LastVisit,
                        LastVisitVal:patient.LastVisitVal,
                        Medications:patient.Medications,
                        Number:patient.Number,
                        IssueDate:patient.IssueDate,
                        IssueDateVal:patient.IssueDateVal,
                       /* Mail:patient.Mail,
                        PDF:patient.PDF*/

                    }
                )
            })

            let _pdata = JSON.parse(JSON.stringify(collection));
            state = {
                ...state,
                initiatePatientsData: _pdata,
                patientsData: _pdata,
                // filterPatientsData: _pdata,
                // patientsCount: collection.length,
                // activePatientsCount: _aadata,
                // pendingPatientsCount: _apdata,
                // lockedPatientsCount: _sldata
            };
            break;
        }
        case NEW_PATIENT: {
            let patient = action.payload
            let patientsData = state.patientsData
            let initiatePatientsData = state.initiatePatientsData

            let newData = [{
                PatientName:patient.PatientName,
                ID:patient.ID,
                Sex: patient.Sex,
                Age:patient.Age,
                TreatmentStartDate:patient.TreatmentStartDate,
                LastVisit:patient.LastVisit,
                Medications:patient.Medications,
                Number:patient.Number,
                IssueDate:patient.IssueDate,
                IssueDateVal:patient.IssueDateVal,
              /*  Mail:patient.Mail,
                PDF:patient.PDF*/
            }]

            let newpatientsData = patientsData.concat(newData)
            let newinitiatePatientsData = initiatePatientsData.concat(newData)

            let _pdata = JSON.parse(JSON.stringify(newpatientsData));
            let _idata = JSON.parse(JSON.stringify(newinitiatePatientsData));
            state = {
                ...state,
                initiatePatientsData:_idata,
                patientsData : _pdata,
                patientsCount: newinitiatePatientsData.length,
                pendingPatientsCount: state.activePatientsCount+1,
            };
            break;
        }
        case CLEAN_PATIENT:{
            state = {
                ...state,
                patientsTableFilter: {},
                selectedPatientsData:[]
            };
            break;
        }
        case FILTER_PATIENTSDATA: {
            let type = action.searchType
            let payload = action.payload
            let patientsTableFilter = state.patientsTableFilter
            let filteredPatientsData = state.initiatePatientsData
            let visibleFilterData = []
            let visiableData = []
            if (type === 'Medications' && payload === 'total'){
                delete patientsTableFilter.Medications
            } else {
                utils.set(patientsTableFilter,type,payload)
            }

            for (const [key, value] of Object.entries(patientsTableFilter)) {

                if (key === 'search'){
                    let val = value.toLowerCase()
                    filteredPatientsData = filteredPatientsData.filter(x =>
                        String(x.PatientName).toLowerCase().indexOf(val) > -1 ||
                        String(x.ID).toLowerCase().indexOf(val) > -1 ||
                        String(x.Sex).toLowerCase().indexOf(val) > -1 ||
                        String(x.Age).toLowerCase().indexOf(val) > -1 ||
                        String(x.TreatmentStartDate).toLowerCase().indexOf(val) > -1 ||
                        String(x.LastVisit).toLowerCase().indexOf(val) > -1 ||
                        String(x.Medications).toLowerCase().indexOf(val) > -1 ||
                        String(x.Number).toLowerCase().indexOf(val) > -1 ||
                        String(x.IssueDate).toLowerCase().indexOf(val) > -1
                    )
                    visibleFilterData = filteredPatientsData
                } else if (key === 'filters') {
                    Object.entries(value).forEach(([label,arr]) => {
                        if (!!arr && label === 'startFrom'){
                            filteredPatientsData = filteredPatientsData.filter(patient => new Date(patient['TreatmentStartDateVal']) > new Date(arr))
                        }
                        if (!!arr && label === 'startTo'){
                            filteredPatientsData = filteredPatientsData.filter(patient => new Date(patient['TreatmentStartDateVal']) > new Date(arr))
                        }
                        if (!!arr && label === 'lastVisitFrom'){
                            filteredPatientsData = filteredPatientsData.filter(patient => new Date(patient['LastVisit']) > new Date(arr))
                        }
                        if (!!arr && label === 'lastVisitTo'){
                            filteredPatientsData = filteredPatientsData.filter(patient => new Date(patient['LastVisit']) > new Date(arr))
                        }
                        if (!!arr && label === 'TreatmentStartDate' && arr.length > 0){
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['Age'] > arr[0] && patient['Age'] < arr[1])
                        }
                        if (!!arr && label === 'LastVisit' && arr.length > 0){
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['Age'] > arr[0] && patient['Age'] < arr[1])
                        }
                        if (!!arr && label === 'Medications' && arr.length > 1){
                            filteredPatientsData = filteredPatientsData.filter(patient => patient.Medications.some(r=> arr[0].indexOf(r) > -1) && patient.Medications.some(r=> arr[1].indexOf(r) > -1))
                        }
                        if (!!arr && label === 'Medications' && arr.length > 0){
                            filteredPatientsData = filteredPatientsData.filter(patient => patient.Medications.some(r=> arr.indexOf(r) > -1))
                        }


                        if (!!arr && label === 'Sex' && arr.length > 0){
                            filteredPatientsData = filteredPatientsData.filter(patient => arr.includes(patient['Sex']))
                        }
                        if (!!arr && label === 'Age' && arr.length > 0){
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['Age'] > arr[0] && patient['Age'] < arr[1])
                        }

                    })
                    switch (value) {

                        case 'active':{
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['AccountStatus'] === 2)
                            break;
                        }
                        case 'pending':{
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['AccountStatus'] === 1)
                            break;
                        }
                        case 'lock':{
                            filteredPatientsData = filteredPatientsData.filter(patient => patient['Enabled'] === 0)
                            break;
                        }
                        default:{
                            break;
                        }
                    }
                    visibleFilterData = filteredPatientsData
                } else {
                        let patients = JSON.parse(JSON.stringify(filteredPatientsData))
                        let valArr = JSON.parse(JSON.stringify(value))


                        if (Array.isArray(valArr)){
                            let res = []
                            valArr.forEach(val => {
                                let patientArr = patients.filter(patient => {
                                    let patientVal = patient[key]
                                    return String(patientVal).toLowerCase() === String(val).toLowerCase()
                                })
                                patientArr.forEach(patient => res.push(patient))
                            })
                            filteredPatientsData = res
                        }
                }
            }
            visiableData = filteredPatientsData

            let _fpdata = JSON.parse(JSON.stringify(visibleFilterData));
            let _vdata = JSON.parse(JSON.stringify(visiableData));
            let _pfdata = JSON.parse(JSON.stringify(patientsTableFilter));
            state = {
                ...state,
                filterPatientsData: _fpdata,
                patientsData: _vdata,
                patientsTableFilter: _pfdata
            };
            break;
        }
        case PATIENTS_SELECTION:{
            let selection = action.payload
            let initiatePatientsData = state.initiatePatientsData

            let newSelection = []
            Object.entries(selection).forEach(([key,value]) => {
                if (value){
                    let index = initiatePatientsData.findIndex((obj => obj.UserID === key));
                    let name = initiatePatientsData[index].FullName
                    newSelection.push({id:key,name})
                }
            })
            state = {
                ...state,
                selectedPatientsData: newSelection
            };
            break;
        }
        case EDIT_PATIENT: {
            let patient = action.payload
            let patientsData = state.patientsData
            let initiatePatientsData = state.initiatePatientsData

            let locked = state.lockedPatientsCount

            let indexData = patientsData.findIndex((obj => obj.UserID === patient.UserID));
            let indexInit = initiatePatientsData.findIndex((obj => obj.UserID === patient.UserID));

            let isLocked = patientsData[indexData]['Enabled'] === 0

            Object.keys(patient).forEach(key => {
                let newVal = patient[key]
                if (patientsData[indexData][key])
                    patientsData[indexData][key] = newVal
                if (initiatePatientsData[indexInit][key])
                    initiatePatientsData[indexInit][key] = newVal
            })

            if (isLocked && patientsData[indexData]['Enabled'] === 1) locked--
            else if (!isLocked && patientsData[indexData]['Enabled'] === 0 ) locked++


            let _pdata = JSON.parse(JSON.stringify(patientsData));
            let _idata = JSON.parse(JSON.stringify(initiatePatientsData));

            let _sldata = JSON.parse(JSON.stringify(locked));

            state = {
                ...state,
                initiatePatientsData:_idata,
                patientsData : _pdata,
                filterPatientsData: _pdata,
                lockedPatientsCount: _sldata
            };
            break;
        }
        case PATIENT_LOCKED_STATUS: {
            let patient = action.payload
            let patientsData = state.patientsData
            let initiatePatientsData = state.initiatePatientsData

            let locked = state.lockedPatientsCount

            let indexData = patientsData.findIndex((obj => obj.UserID === patient.UserID));
            let indexInit = initiatePatientsData.findIndex((obj => obj.UserID === patient.UserID));

            let isLocked = patientsData[indexData]['Enabled'] === 0

            let Enabled = patient.PII.User.Enabled

            patientsData[indexData]['Enabled'] = Enabled
            initiatePatientsData[indexInit]['Enabled'] = Enabled

            if (isLocked && patientsData[indexData]['Enabled'] === 1) locked--
            else if (!isLocked && patientsData[indexData]['Enabled'] === 0 ) locked++

            let _pdata = JSON.parse(JSON.stringify(patientsData));
            let _idata = JSON.parse(JSON.stringify(initiatePatientsData));

            let _sldata = JSON.parse(JSON.stringify(locked));
            state = {
                ...state,
                initiatePatientsData:_idata,
                patientsData : _pdata,
                filterPatientsData: _pdata,
                lockedPatientsCount: _sldata
            };
            break;
        }
        case DELETE_PATIENT: {
            let patientId = action.payload
            let patientsData = state.patientsData
            let initiatePatientsData = state.initiatePatientsData
            let locked = state.lockedPatientsCount
            let active = state.activePatientsCount
            let pending = state.pendingPatientsCount

            let indexData = patientsData.findIndex((obj => obj.UserID === patientId));
            let isLocked = patientsData[indexData]['Enabled'] === 0

            let isActive = patientsData[indexData]['AccountStatus'] === 2
            let isPending = patientsData[indexData]['AccountStatus'] === 1

            let newPatientData = patientsData.filter(patient => patient.UserID !== patientId)
            let newInitiatePatientsData = initiatePatientsData.filter(patient => patient.UserID !== patientId)

            if (isLocked) locked--
            if (isActive) active--
            if (isPending) pending--

            let _pdata = JSON.parse(JSON.stringify(newPatientData));
            let _idata = JSON.parse(JSON.stringify(newInitiatePatientsData));

            let _aadata = JSON.parse(JSON.stringify(active));
            let _apdata = JSON.parse(JSON.stringify(pending));
            let _sldata = JSON.parse(JSON.stringify(locked));
            state = {
                ...state,
                initiatePatientsData:_idata,
                patientsData : _pdata,
                filterPatientsData: _pdata,
                patientsCount: newInitiatePatientsData.length,
                activePatientsCount: _aadata,
                pendingPatientsCount: _apdata,
                lockedPatientsCount: _sldata
            };
            break;
        }
        case PATIENTS_STATUSES: {

            break;
        }
        default:
            break;
    }
    return state;
};
export default patientsTableDataReducer;
