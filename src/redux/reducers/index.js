import { combineReducers } from 'redux'

import ecpTableDataReducer from './admin/ecpTableDataReducer'
import siteTableDataReducer from './admin/siteTableDataReducer'
import deviceTableDataReducer from './admin/deviceTableDataReducer'
import adminTableDataReducer from './admin/adminTableDataReducer'
import versionTableDataReducer from './admin/versionTableDataReducer'
import patientsTableDataReducer from './doctor/patientTableDataReducer'
import patients_archiveTableDataReducer from './doctor/patients_archiveTableDataReducer'

export default combineReducers({
    ecpTableDataReducer,
    siteTableDataReducer,
    deviceTableDataReducer,
    adminTableDataReducer,
    versionTableDataReducer,
    patientsTableDataReducer,
    patients_archiveTableDataReducer,
})
