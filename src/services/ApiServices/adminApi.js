import {authenticationService} from "../AuthenticationService";
import axios from "axios";
import Config from "../../config/Config";

const adminApi = {

    AddNewSite: async (site) => {

        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;
        console.log(SystemAdmin.profile)

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/sites/signup`,
                site,
                requestconfig
            ));
    },
    EditSite: async (site_data,SiteID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/sites/${SiteID}/edit `,
                site_data,
                requestconfig
            ));
    },
    getAllSites: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/sites/getSites `,
                requestconfig
            );

    },

    AddNewECP: async (ecp) => {
        console.log("ECP: ", ecp)
        if (!authenticationService.currentUserValue)
            return;

        let newECP = Object.assign({},ecp,{WithoutMail:true})

        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/doctors/signup`,
                newECP,
                requestconfig
            ));
    },

    EditECP: async (ecp_data,ECP_UserID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/doctors/${ECP_UserID}/edit`,
                ecp_data,
                requestconfig
            ));
    },
    getAllECPs: async () => {
        console.log('getAllECPs')
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/doctors/getHCPs `,
                requestconfig
            );

    },
    AddNewAdmin: async (admin) => {
        if (!authenticationService.currentUserValue)
            return;
        let newAdmin = Object.assign({},admin,{WithoutMail:true})

        let SystemAdmin = authenticationService.currentUserValue;

        let requestconfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': authenticationService.currentUserValue.token,
            },
        };
        return await axios.post(
            `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/admins/signup`,
            newAdmin,
            requestconfig)

    },
    EditAdmin: async (admin_data,Admin_UserID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/admins/${Admin_UserID}/edit`,
                admin_data,
                requestconfig
            ));
    },
    deleteAdmin: async (Admin_UserID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.delete(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/admins/${Admin_UserID}/delete`,
                requestconfig
            ));
    },
    getAllAdmins: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/callCenter/admins/${SystemAdmin.profile.UserID}/admins/getAdmins`,
                requestconfig
            );

    },
    getAllDevices: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/devices/getAllDevices`,
                requestconfig
            );

    },
   /* AddNewVersion: async (version) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/versions/addNewVersion`,
                version,
                requestconfig
            ));
    },
    editVersion: async (version_data) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/versions/editVersion`,
                version_data,
                requestconfig
            ));
    },
    getAllVersions: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/versions/getAllVersions`,
                requestconfig
            );

    },
    deleteVersion: async (VersionID) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.delete(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/versions/${VersionID}`,
                requestconfig
            ));
    },*/
    sendMessageToPatients: async (data) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/patients/sendMessages`,
                data,
                requestconfig
            ));
    },
    sendMessageToEcp: async (data) => {
        if (!authenticationService.currentUserValue)
            return;
        let SystemAdmin = authenticationService.currentUserValue;

            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return (await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeSwiftAdmins/${SystemAdmin.profile.UserID}/doctors/sendMessages`,
                data,
                requestconfig
            ));
    },
}

export default adminApi;
