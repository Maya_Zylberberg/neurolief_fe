import {authenticationService} from "../AuthenticationService";
import axios from "axios";
import Config from "../../config/Config";

const doctorApi = {
    newPatient: async (patient_data) => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };

            return await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/patients/signup`,
                patient_data,
                requestconfig
            );

    },

    getAllPatients: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/getallpatients`,
                requestconfig
            );
},
    editPatient: async (patient_data,patient_id) => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };

            return await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/patients/${patient_id}/profile`,
                patient_data,
                requestconfig
            );

    },
    getLastVisitByPatientId: async (patient_id) => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };

            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/patients/${patient_id}/getLastVisit`,
                requestconfig
            );
},
    addNewPatientVisit: async (patient_data,patient_id) => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };

            return await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/patients/${patient_id}/addNewVisit`,
                patient_data,
                requestconfig
            );

    },
    gePatientById: async (patient_id) => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/patients/${patient_id}/profile`,
                requestconfig
            );
},
    deletePatient: async (patient_id) => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };

            return await axios.delete(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/patients/${patient_id}/profile`,
                requestconfig
            );

    },
    sendMessageToPatients: async (data) => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };

            return await axios.post(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/patients/sendMessages`,
                data,
                requestconfig
            );

    },
    getAllCompletedPatients: async () => {
        if (!authenticationService.currentUserValue)
            return;
        let doctor = authenticationService.currentUserValue;
            let requestconfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': authenticationService.currentUserValue.token,
                },
            };
            return await axios.get(
                `${Config.globalUrl}/api/v1/webapp/eyeCareProviders/${doctor.profile.UserID}/getallpatients?archive=true`,
                requestconfig
            );
},
}

export default doctorApi;
