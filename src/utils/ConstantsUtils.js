import Constants from "./constants";


class ConstantsUtils {

    getVaConstantsValues (VaConstant){
        if (VaConstant === 1 || VaConstant === '1') return Constants.LogMAR
        else if (VaConstant === 4 || VaConstant === '4') return Constants.Decimal
        else if (VaConstant === 2 || VaConstant === '2') return Constants.Feet
        else if (VaConstant === 3 || VaConstant === '3') return Constants.Meter
    }

    getVaConstant (VaConstant){
        if (VaConstant !== undefined)
        return  Constants.VaConstants.find(VaConst => VaConst.value === VaConstant.toString())
    }

    getVaConstantLabel (VaConstant){
        if (VaConstant !== undefined)
            return  Constants.VaConstants.find(VaConst => VaConst.value === VaConstant.toString()).label
    }

    convertVaValuesOptions (from,to,value) {
        let VaConstants = Constants.VaConstants
        let VaConstFrom = VaConstants.find(elem => elem.value === from.toString())
        let VaConstTo = VaConstants.find(elem => elem.value === to.toString())
        let index = Constants[VaConstFrom.label].findIndex(con => con.value === value.toString())
        return Constants[VaConstTo.label][index].value
    }

    convertToLogMAR (from, value) {
        let VaConstants = Constants.VaConstants
        let VaConstFrom = VaConstants.find(elem => elem.value === from.toString())
        let VaConstTo = VaConstants.find(elem => elem.value === "1")
        let index = Constants[VaConstFrom.label].findIndex(con => con.value === value.toString())
        return Constants[VaConstTo.label][index].value
    }

    convertFromLogMAR (to, value) {
        if (to !== undefined && value !== undefined){
            if (value === 0 || value === '0') value = parseFloat('0').toFixed(1)
            if (value === 1 || value === '1') value = parseFloat('1').toFixed(1)
            let VaConstants = Constants.VaConstants
            let VaConstFrom = VaConstants.find(elem => elem.value === "1")
            let VaConstTo = VaConstants.find(elem => elem.value === to.toString())
            let index = Constants[VaConstFrom.label].findIndex(con => con.value === value.toString())
            return Constants[VaConstTo.label][index]
        }
    }

    getBooksStereoTestOptions (book) {
        if (book === 1 || book === '1') return Constants.SFT
        else if (book === 2 || book === '2') return Constants.RD
        else if (book === 3 || book === '3') return Constants.RP
        else if (book === 4 || book === '4') return Constants.TBT
    }

    getStereoTestsValuesOptions (book,stereoTest) {
        if (stereoTest){
            if (book === 1 || book === '1') return Constants[`SFT${stereoTest.toString()}`]
            else if (book === 2 || book === '2') return Constants[`RD${stereoTest.toString()}`]
            else if (book === 3 || book === '3') return Constants[`RP${stereoTest.toString()}`]
            else if (book === 4 || book === '4') return Constants[`TBT${stereoTest.toString()}`]
        } else return []
    }

    getECP_AccountStatus (val) {
        return Constants.ECP_AccountStatus.find(status => status.value === val).label
    }

    getPatient_AccountStatus (val) {
        return Constants.Patient_AccountStatus.find(status => status.value === val).label
    }

    getAmblyopicEye = (AmblyopicEyeValue) => {
        if (AmblyopicEyeValue !== undefined) {
            return Constants.AmblyopicEye.find(Eye => Eye.value === AmblyopicEyeValue).label
        }
    }

    getVersionTypeByVal = (val) => {
        if (val !== null && val !== undefined)
            return Constants.versions.find(version => version.value === val).label
    }
}

export default new ConstantsUtils();
