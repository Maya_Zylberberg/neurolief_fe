let  Constants = {
    VaConstants: [
        {label: "LogMAR", value: "1"},
        {label: "Decimal", value: "4"},
        {label: "Feet", value: "2"},
        {label: "Meter", value: "3"}
    ],

    LogMAR : [
        {label: "1.3", value: "1.3"},
        {label: "1.2", value: "1.2"},
        {label: "1.1", value: "1.1"},
        {label: "1.0", value: "1.0"},
        {label: "0.9", value: "0.9"},
        {label: "0.8", value: "0.8"},
        {label: "0.7", value: "0.7"},
        {label: "0.6", value: "0.6"},
        {label: "0.5", value: "0.5"},
        {label: "0.4", value: "0.4"},
        {label: "0.3", value: "0.3"},
        {label: "0.2", value: "0.2"},
        {label: "0.1", value: "0.1"},
        {label: "0.0", value: "0.0"},
        {label: "-0.1", value: "-0.1"},
        {label: "-0.2", value: "-0.2"},
        {label: "-0.3", value: "-0.3"}
    ],

    Decimal: [
        {label: "0.050", value: "0.05"},
        {label: "0.063", value: "0.063"},
        {label: "0.080", value: "0.08"},
        {label: "0.10", value: "0.1"},
        {label: "0.12", value: "0.12"},
        {label: "0.16", value: "0.16"},
        {label: "0.20", value: "0.2"},
        {label: "0.25", value: "0.25"},
        {label: "0.32", value: "0.32"},
        {label: "0.40", value: "0.4"},
        {label: "0.50", value: "0.5"},
        {label: "0.63", value: "0.63"},
        {label: "0.80", value: "0.8"},
        {label: "1.00", value: "1"},
        {label: "1.25", value: "1.25"},
        {label: "1.60", value: "1.6"},
        {label: "2.00", value: "2"}
    ],

    Feet : [
        {label: "20/400", value: "20/400"},
        {label: "20/320", value: "20/320"},
        {label: "20/250", value: "20/250"},
        {label: "20/200", value: "20/200"},
        {label: "20/160", value: "20/160"},
        {label: "20/125", value: "20/125"},
        {label: "20/100", value: "20/100"},
        {label: "20/80", value: "20/80"},
        {label: "20/63", value: "20/63"},
        {label: "20/50", value: "20/50"},
        {label: "20/40", value: "20/40"},
        {label: "20/32", value: "20/32"},
        {label: "20/25", value: "20/25"},
        {label: "20/20", value: "20/20"},
        {label: "20/16", value: "20/16"},
        {label: "20/12.5", value: "20/12.5"},
        {label: "20/10", value: "20/10"}
    ],

    Meter: [
        {label: "6/120", value: "6/120"},
        {label: "6/95", value: "6/95"},
        {label: "6/75", value: "6/75"},
        {label: "6/60", value: "6/60"},
        {label: "6/48", value: "6/48"},
        {label: "6/38", value: "6/38"},
        {label: "6/30", value: "6/30"},
        {label: "6/24", value: "6/24"},
        {label: "6/19", value: "6/19"},
        {label: "6/15", value: "6/15"},
        {label: "6/12", value: "6/12"},
        {label: "6/9.5", value: "6/9.5"},
        {label: "6/7.5", value: "6/7.5"},
        {label: "6/6.0", value: "6/6.0"},
        {label: "6/4.8", value: "6/4.8"},
        {label: "6/3.8", value: "6/3.8"},
        {label: "6/3.0", value: "6/3.0"}
    ],

    Books: [
        {label: "Stereo Fly test", value: 1},
        {label: "Random Dot", value: 2},
        {label: "RANDOT PRESCHOOL", value: 3},
        {label: "tereo Butterfly test", value: 4}
    ],

    SFT: [
        {label: "Fly wings", value: 1},
        {label: "Circles", value: 2},
        {label: "Animals", value: 3},
    ],
    RD: [
        {label: "Circles", value: 1},
        {label: "Random dots- lea symbols", value: 2},
        {label: "Lea symbols", value: 3},
    ],
    RP: [
        {label: "Test 1", value: 1},
        {label: "Test 2", value: 2},
        {label: "Test 3", value: 3},
    ],
    TBT: [
        {label: "Top of upper wings", value: 1},
        {label: "Bottom of lower wings", value: 2},
        {label: "Tip of abdomen", value: 3},
        {label: "Circles", value: 4},
        {label: "Animals", value: 5},
    ],

    SFT1:[
        {label: 3552, value: 3552}
    ],
    SFT2:[
        {label: 800, value: 800},
        {label: 400, value: 400},
        {label: 200, value: 200},
        {label: 140, value: 140},
        {label: 100, value: 100},
        {label: 80, value: 80},
        {label: 60, value: 60},
        {label: 50, value: 50},
        {label: 40, value: 40},
    ],
    SFT3:[
        {label: 400, value: 400},
        {label: 200, value: 200},
        {label: 100, value: 100}
    ],

    RD1: [
        {label: 400, value: 400},
        {label: 200, value: 200},
        {label: 160, value: 160},
        {label: 100, value: 100},
        {label: 63, value: 63},
        {label: 50, value: 50},
        {label: 40, value: 40},
        {label: 32, value: 32},
        {label: 25, value: 25},
        {label: 20, value: 20},
        {label: 16, value: 16},
        {label: 12.5, value: 12.5},
    ],
    RD2: [
        {label: 500, value: 500},
        {label: 250, value: 250},
        {label: 125, value: 125},
        {label: 63, value: 63}
    ],
    RD3: [
        {label: 400, value: 400},
        {label: 200, value: 200},
        {label: 100, value: 100}
    ],

    RP1: [
        {label: 200, value: 200},
        {label: 100, value: 100}
    ],
    RP2: [
        {label: 60, value: 60},
        {label: 40, value: 40},
    ],
    RP3: [
        {label: 800, value: 800},
        {label: 400, value: 400}
    ],

    TBT1: [
        {label: 2000, value: 2000}
    ],
    TBT2: [
        {label: 1150, value: 1150}
    ],
    TBT3: [
        {label: 700, value: 700}
    ],
    TBT4: [
        {label: 800, value: 800},
        {label: 400, value: 400},
        {label: 200, value: 200},
        {label: 140, value: 140},
        {label: 100, value: 100},
        {label: 80, value: 80},
        {label: 60, value: 60},
        {label: 50, value: 50},
        {label: 40, value: 40},
    ],
    TBT5: [
        {label: 400, value: 400},
        {label: 200, value: 200},
        {label: 100, value: 100}
    ],

    Stories2: {
        'tereo Butterfly test':{
            'Top of upper wings':[2000],
            'Bottom of lower wings':[1150],
            'Tip of abdomen':[700],
            'Circles': [800,400,200,140,100,80,60,50,40],
            'Animals':[400,200,100]

        }
    },

    weeklyNumberOfSessions: [
        {label: 4, value: 4},
        {label: 6, value: 6},
        {label: 7, value: 7}
    ],

    Profession: [
        {label: "MD", value: 1},
        {label: "Optometrist", value: 2},
        {label: "Ophthalmologist", value: 3},
        {label: "Orthoptist", value: 4},
        {label: "Optician", value: 5},
        {label: "Administrative", value: 6},
        {label: "Other", value: 7}
    ],

    EyeDevisionSuffixes: [
        {label: "Phoria", value: 1},
        {label: "Tropia", value: 2},
        {label: "Microtropia ", value: 3},
    ],

    Bases: [
        {label: "Up", value: 1},
        {label: "Down", value: 2},
        {label: "Out ", value: 3},
    ],

    Genders: [
        {label: "Male", value: 1},
        {label: "Female", value: 2},
        {label: "Other", value: 3},
    ],

    ECP_AccountStatus : [
        {label: "Not active", value: 0},
        {label: "Active", value: 1},
    ],

    Patient_AccountStatus : [
        {label: "Pending Pairing", value: 1},
        {label: "Active", value: 2},
        {label: "Completed", value: 3}
    ],

    AmblyopicEye : [
        {label: "Left Eye", value: 1},
        {label: "Right Eye", value: 2},
    ],

    emailRegExp: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    phoneRegExp: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,

    versions: [
        {value: "WIN", label: 'Windows'},
        {value: "ESAPP", label: 'ESAPP'},
        {value: "ET", label: 'ET Firmware'},
        {value: "EMITTER", label: 'Emitter Firmware'},
        {value: "TV", label: 'Teamviewer'}
    ],

    treatmentHistoryType: [
        {value: 1, label: 'Patch'},
        {value: 2, label: 'None'},
        {value: 3, label: 'Atropine'},
        {value: 3, label: 'Other'}
    ],

    locked: [
        {value: 0, label: 'Locked'},
        {value: 1, label: 'Unlocked'}
    ],
}

export default Constants

