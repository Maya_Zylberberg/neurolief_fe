import React, {Component} from 'react'
import {authenticationService} from '../../services/AuthenticationService';

import SvGraphics from "../../assets/SvGraphics";
import * as Yup from "yup";
import validationHelper from "../../utils/validationHelper";
import utils from "../../utils/utils";
import Error from "../../components/single_components/Error";
import Config from "../../config/Config";
import {compose} from "redux";
import {withTranslation} from "react-i18next";

class CreatePasswordComposed extends Component {
    constructor(props) {
        super(props);

        if (!authenticationService.pwdUserValue) {
            this.props.history.push('/');
        }

        this.state = {
            password1:'',
            passwordConfirm: '',
            auth: {},
            isErr:false,
            errMessage:'',
            errors: {},
            errorsOnSave:{},
        }
    }

    initValidation = async () => {
        let {t} = this.props

        let validationSchema = Yup.object().shape({
            password1: Yup.string().matches(Config.passwordRegExp,t('defaults.crt_pwd.pwd_validation')).required(t('defaults.crt_pwd.pwd_req')),
            passwordConfirm: Yup.string().oneOf([Yup.ref('password1'), null], t('defaults.crt_pwd.pwd_match')).test('match', t('defaults.crt_pwd.pwd_match'), function (password1) {
                return password1 === this.parent.passwordConfirm
            }).required(t('defaults.crt_pwd.pwd_confirm_req'))
        })
        let errors = await validationHelper.validate(this.state.auth,validationSchema)
        await this.setState({
            validationSchema,
            errors
        })
    }

    async componentWillMount()/*: void*/ {
        await this.initValidation()
    }

    onSubmit = async () => {
        if (!utils.isEmpty(this.state.errors)){
            this.setState({errorsOnSave:this.state.errors});
            return;
        }
        let {password1} = this.state
        let response = await authenticationService.changeNewPassword(password1);
        if (response.status < 400) {
            let data = response.data
            if (data && data.hasOwnProperty('newPassword') && data['newPassword'] === true) {
                const {from} = {from: {pathname: "/dashboard"}};
                this.props.history.push(from);
            } else {
                await this.setPassword1({target: {value: '',name:'password1'}})
                await this.setpasswordConfirm({target: {value: '',name:'passwordConfirm'}})
                this.setState({
                    password1:'',
                    passwordConfirm:'',
                    isErr:true,
                    errMessage:response.data?response.data.message:'No connection to server',
                })
            }
        } else {
            this.setState({
                password1:'',
                passwordConfirm:'',
                isErr:true,
                errMessage:response.data?response.data.message:'No connection to server',
            })
        }
    }

    setPassword1 = async (e) => {
        let {auth,validationSchema} = this.state
        let name = 'password1'
        this.setState({password1:e.target.value})
        utils.set(auth,name,e.target.value)
        let errors = await validationHelper.validate(auth, validationSchema);
        let errorsOnSave = this.state.errorsOnSave;
        if (utils.get(errorsOnSave,name)){
            utils.set(errorsOnSave,name,utils.get(errors,name))
        }
        this.setState({
            auth,
            errors,
            errorsOnSave,
            isErr:false,
            errMessage:''
        })
    }

    setPasswordConfirm = async (e) => {
        let {auth,validationSchema} = this.state
        let name = 'passwordConfirm'
        this.setState({passwordConfirm:e.target.value})
        utils.set(auth,name,e.target.value)
        let errors = await validationHelper.validate(auth, validationSchema);
        let errorsOnSave = this.state.errorsOnSave;
        if (utils.get(errorsOnSave,name)){
            utils.set(errorsOnSave,name,utils.get(errors,name))
        }
        this.setState({
            auth,
            errors,
            errorsOnSave,
            isErr:false,
            errMessage:''
        })
    }

    render() {
        let {t} = this.props
        return (
            <div className={"login-page"}>
                <div className={'h-centered eye-swift-login-logo-container'}>
                    <SvGraphics svgname={'eye_swift_blue'} className={'eye-swift-login-logo'} style={{width:'340px'}}/>
                </div>
                <div className={'centered eye-swift-login-main-board'}>
                    <label className={'eye-swift-login-label'}>{t('defaults.crt_pwd.new_pwd')}</label>
                    <label className={'h-centered eye-swift-newPassword-label'}>{t('defaults.crt_pwd.new_pwd_info')}</label>
                    <div className={'eye-swift-login-form-container'}>
                        <div className={'h-centered eye-swift-login-form'}>
                            <Error isNonFloat={true} errorMessage={utils.get(this.state.errorsOnSave ,'password1')} isPasswordValidationErr={true} isShown={!!utils.get(this.state.errorsOnSave ,'password1')}/>
                            <div className={"login-input-wrapper form-group eye-swift-login-form-group"}>
                                <input className={"nova-input nova-form-input eye-swift-login-input-field"}
                                       placeholder={t('defaults.crt_pwd.pwd')}
                                       type={"password"}
                                       name={"password1"}
                                       value={this.state.password1}
                                       onChange={e => this.setPassword1(e)}
                                       onKeyPress={event => {
                                           if (event.key === 'Enter') {
                                               this.onSubmit()
                                           }
                                       }}/>
                                <SvGraphics className={"input-pic"} svgname={'lock'} style={{width: '20px', height: '20px'}}/>
                            </div>
                            <Error isNonFloat={true} errorMessage={utils.get(this.state.errorsOnSave ,'passwordConfirm')} isShown={!!utils.get(this.state.errorsOnSave ,'passwordConfirm')}/>
                            <div className={"login-input-wrapper form-group eye-swift-login-form-group"}>
                                <input className={"nova-input nova-form-input eye-swift-login-input-field"}
                                       placeholder={t('defaults.crt_pwd.pwd_valid_lbl')}
                                       type={"password"}
                                       name={"passwordConfirm"}
                                       value={this.state.passwordConfirm}
                                       onChange={e => this.setPasswordConfirm(e)}
                                       onKeyPress={event => {
                                           if (event.key === 'Enter') {
                                               this.onSubmit()
                                           }
                                       }}/>
                                <SvGraphics className={"input-pic"} svgname={'lock'} style={{width: '20px', height: '20px'}}/>
                            </div>
                            <div className="form-group eye-swift-login-form-group">
                                <button className="btn h-centered btn-primary eye-swift-login-form-button" onClick={this.onSubmit}>{t('defaults.crt_pwd.submit')}</button>
                            </div>

                        </div>
                    </div>
                    <div className={'eye-swift-login-form-group loginError red h-centered'}>
                        <Error isNonFloat={true} errorMessage={t('defaults.crt_pwd.auth_err') + ': ' + this.state.errMessage} isShown={this.state.isErr}/>
                    </div>
                    {/*<div className={'h-centered eye-swift-login-footer-container'}*/}
                    {/*     onClick={e => this.props.history.goBack()}>*/}
                    {/*    <a>Back to Login</a>*/}
                    {/*</div>*/}
                </div>
                <div className={'h-centered nova-sight-login-logo-container'}>
                    <SvGraphics svgname={'nova_sight'} className={'nova-sight-login-logo'} style={{width:'183px'}}/>
                </div>

                <div className={'login-version-element'}>
                    <label className={'login-version-element-label'}>Version:</label>
                    <label className={'login-version-element-value'}>{Config.version}</label>
                </div>
            </div>

        )
    }
}

let CreatePassword = compose(
    withTranslation()
)(CreatePasswordComposed)

export default CreatePassword;
