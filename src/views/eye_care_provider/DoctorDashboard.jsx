import React, {Component} from 'react'
import {history} from "../../utils/history";

import '../../style/NovaInfo.css'
import '../../style/rtl_css/NovaInfo.css'
import {connect} from "react-redux";
import {
    filterPatientsTable
} from "../../redux/actions/Tabs-Actions";
import {compose} from "redux";
import {withTranslation} from "react-i18next";

import '../../style/doctorStyles/ClinicDashboard.css';
import '../../style/doctorStyles/rtl_css/clinicDashboard.css';

import '../../style/doctorStyles/MyAdminDashboard.css';
import '../../style/doctorStyles/rtl_css/myAdminDashboard.css';

import PatientsDashboard from './doctor-tabs/doctorComponents/PatientsDashboard'
import ClinicalDashboard from './doctor-tabs/doctorComponents/ClinicalDashboard'
import AllPatients from './doctor-tabs/doctorComponents/AllPatients'
import PatientTable from './doctor-tabs/doctorComponents/PatientTable'


class DoctorDashboard /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "title"
        }
    }

    componentDidMount() {
        this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }

    render() {
        let {data} = this.props
        return (
            <div className={'totalBackground '}>
                <div className={'adminMargin'}>
                    {/*<PatientsDashboard/>*/}
                    <AllPatients/>
                    {/*<PatientTable/>*/}
                </div>
            </div>
        )
    }
}

export default DoctorDashboard;
