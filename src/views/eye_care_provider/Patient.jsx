import React, {Component} from 'react'
import ReturnNav from "../../components/single_components/ReturnNav";
import PatientHeader from "../../components/patient_profile/PatientHeader";
import '../../components_style/PatientScreen.css'
import '../../components_style/rtl_css/PatientScreen.css'
import {doctorApi} from "../../services/ApiService";
import PatientPersonal from "../../components/patient_profile/PatientPersonal";
import PatientVisitInfo from "../../components/patient_profile/PatientVisitInfo";
import PatientVisitTimeLine from "../../components/patient_profile/PatientVisitTimeLine";
import utils from "../../utils/utils";
import {
    deletePatient,
    addNewArchivePatient
} from "../../redux/actions/Tabs-Actions";
import {connect} from "react-redux";
import {history} from "../../utils/history";
// import PatientProfileVisualAcuity from "../../components/patient_profile/PatientProfileVisualAcuity";
// import PatientProfileStereoAcuity from "../../components/patient_profile/PatientProfileStereoAcuity";
// import PatientProfileVisitHistory from "../../components/patient_profile/PatientProfileVisitHistory";
// import PatientProfilePrescription from "../../components/patient_profile/PatientProfilePrescription";
// import PatientProfileTreatmentHistory from "../../components/patient_profile/PatientProfileTreatmentHistory";
import SvGraphics from "../../assets/SvGraphics";
import GraphNavigator from "../../components/patient_profile/GraphNavigator";


class PatientConnected extends Component {

    constructor(props) {
        super(props);
        this.state = {
            patientId:null,
            patientName: null,
            patient: null,
            lastVisit: null,
            visits: [],
            completed: false,
            isAdvanced: false
        }
    }


    componentDidMount = async () => {
        let rowInfo = this.props.location.state.rowInfo
        let patientId = rowInfo.id
        let patientName = rowInfo.FullName
        let completed = this.props.location.state.completed
        let response = await doctorApi.gePatientById(patientId)

        let patient = response && response.hasOwnProperty('data') ? response.data : {}
        utils.set(patient,'Status',rowInfo.Status)
        let visits = patient && patient.Visits && Array.isArray(patient.Visits)? patient.Visits.reverse() : []
        let lastVisit = patient && patient.hasOwnProperty('Visits') ? patient.Visits[patient.Visits.length-1] : {}
        lastVisit['NextVisit'] = !!patient ? patient.NextVisit : null
        this.setState({patientId,patientName,patient,lastVisit,visits,completed})
    }

    onNavBack = () => {
        if (!this.state.completed) {
            history.push({
                pathname: `/dashboard/patients`,
                state: {
                    navTabTo: 0,
                    from: {
                        fromPath: `/dashboard/Patient`,
                    }
                },
            })
        }
        else {
            history.push({
                pathname: `/dashboard/archive`,
                state: {
                    navTabTo: 1,
                    from: {
                        fromPath: `/dashboard/Patient`,
                    }
                },
            })
        }
    }

    onNextVisitChange(date){
        let lastVisit = this.state.lastVisit
        utils.set(lastVisit,'NextVisit',date)
        this.setState({lastVisit})
    }

    deletePatient = (patientID) => {
        this.props.deletePatient(patientID)
    }

    AdvancedToggle = () => {
        let {isAdvanced} = this.state
        this.setState({
            isAdvanced: !isAdvanced
        })
    }

    render() {
        let patientId =  this.state.patientId
        let patientName =  this.state.patientName
        let patient = this.state.patient
        let lastVisit = this.state.lastVisit
        let DateOfActivation = patient ? patient.DateOfActivation : null
        return (
            <div className={'Patient-Screen'}>
                <ReturnNav onNavBackClick={this.onNavBack.bind(this)}/>
                <div className={'nova-patient-screen-header'}>
                    {
                        patientName && patientId &&
                        <PatientHeader addNewArchivePatient={patient => this.props.addNewArchivePatient(patient)} completed={this.state.completed} patientName={patientName} patientId={patientId} onNavBack={this.onNavBack.bind(this)}/>
                    }
                </div>
                <div className={'nova-patient container'}>
                    <PatientPersonal lastVisit={lastVisit}
                                     completed={this.state.completed}
                                     patientName={patientName}
                                     patientId={patientId}
                                     patient={patient}
                                     deletePatient={userId => this.deletePatient(userId)}
                                     onNavBack={this.onNavBack.bind(this)}/>
                    <div className={'nova-patient-right inline'}>
                        <div className={'nova-patient-right-element nova-patient-right-personal'}>
                            <PatientVisitInfo lastVisit={lastVisit} DateOfActivation={DateOfActivation}/>
                            <PatientVisitTimeLine completed={this.state.completed} lastVisit={lastVisit} FullName={patientName} patientId={patientId} onNextVisitChange={e => this.onNextVisitChange(e)}/>
                        </div>

                        <div className={'nova-patient-right-element nova-patient-right-acuity'}>
                            {/*<PatientProfileVisualAcuity visits={this.state.visits}/>*/}
                            {/*<PatientProfileStereoAcuity visits={this.state.visits}/>*/}
                            <GraphNavigator/>
                        </div>


                        {/*<div className={'more-info-controller'} style={{display: this.state.isAdvanced? 'block': 'none'}}>*/}
                        {/*    <div className={'patient-profile-more-info'}>*/}
                        {/*        <div className={'nova-patient-right-element nova-patient-right-lastVisit-history'}>*/}
                        {/*            /!*<PatientProfileVisitHistory lastVisit={lastVisit} visits={this.state.visits}/>*!/*/}
                        {/*        </div>*/}
                        {/*        <div className={'nova-patient-right-element nova-patient-right-prescription'}>*/}
                        {/*            /!*<PatientProfilePrescription lastVisit={this.state.lastVisit}/>*!/*/}
                        {/*        </div>*/}
                        {/*            /!*<PatientProfileTreatmentHistory lastVisit={lastVisit}/>*!/*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                        {/*<div className={'patient-form-advanced-option'}>*/}
                        {/*    <div className={'patient-profile-more-switch'} onClick={this.AdvancedToggle.bind(this)}>*/}
                        {/*        <SvGraphics className={'inline v-centered'} svgname={`advanced-${this.state.isAdvanced?'opened':'closed'}`} height={'30px'} width={'30px'}/>*/}
                        {/*        <label className={'inline advanced-options v-centered'}>{this.state.isAdvanced?'See Less':'See More'}</label>*/}
                        {/*    </div>*/}
                        {/*</div>*/}
                    </div>
                </div>
            </div>

        )
    }
}


function mapDispatchToProps(dispatch) {
    return {
        deletePatient: patientID => dispatch(deletePatient(patientID)),
        addNewArchivePatient: patientData => dispatch(addNewArchivePatient(patientData))
    };
}
const mapStateToProps = () => {
    return {

    };
};
const Patient = connect(
    mapStateToProps,
    mapDispatchToProps
)(PatientConnected);

export default Patient;
