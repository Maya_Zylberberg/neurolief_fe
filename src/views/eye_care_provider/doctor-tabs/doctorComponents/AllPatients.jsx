import React from 'react';
import {HiOutlineSearch} from "react-icons/hi";
import {Nav} from "react-bootstrap";
import {FaFilter} from "react-icons/fa";
import {AiFillWarning} from "react-icons/ai";
import Filter from '../../../../components/doctor/Filter';
import {withTranslation} from "react-i18next";
import {filterPatientsTable} from "../../../../redux/actions/Tabs-Actions";

import {compose} from "redux";
import {connect} from "react-redux";
import ExistingPatient from "../../../../components/doctor/ExistingPatient";
import NewPrescription from "../../../../components/doctor/NewPrescription";
import Abortive from "../../../../components/doctor/PatientInfo/Abortive";
import Preventive from "../../../../components/doctor/PatientInfo/Preventive";
import PatientTable from "./PatientTable";
import PrescriptionsTable from "./PrescriptionsTable";



class AllPatientsComposed extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            menuOpen: false,
            isSideBarOpen:false,
            isExistingPatientOpen:false,
            isPrescriptionOpen: false,
            view: 1


        }
    }

    componentDidMount() {
    }

    componentWillUnmount() {

    }
    changeView1 = () => {
        this.setState({view: 1});
    }

    changeView2 = () => {
        this.setState({view: 2});
    }



    openSideBar(){
        document.body.style.overflow = "hidden"
        this.setState({isSideBarOpen:true})
    }
    openExisting(){
        document.body.style.overflow = "hidden"
        this.setState({isExistingPatientOpen:true})
    }
    openPrescription(){
        console.log("openPrescription")
        document.body.style.overflow = "hidden"
        this.setState({isPrescriptionOpen:true})
    }

    openMenu() {
        this.setState({ menuOpen: true })
    }

    closeMenu() {
        this.setState({ menuOpen: false })
    }


    render() {
        let {isSideBarOpen, isExistingPatientOpen, isPrescriptionOpen} = this.state
        let {t} = this.props
        let { view } = this.state


        return (
            <div>
                {/*<div className={'allPatientBox'}>*/}
                <div className={'topAllPatientArea'}>
                    <div>
                        <button className={view === 1 ? 'patientsTabsButton': 'prescriptionTabsButton'}  onClick={this.changeView1} type="button" data-toggle="modal"
                                data-target="#exampleModal"  > {t('patient.dashboard.Patients')}
                        </button>
                        <button className={view === 2 ? 'patientsTabsButton': 'prescriptionTabsButton'}  onClick={this.changeView2} type="button" data-toggle="modal"
                                data-target="#exampleModal"  > {t('patient.dashboard.Prescriptions')}
                        </button>
                    </div>
                    <div>
                        <button className={"patientsButtonDashboard"} onClick={this.openPrescription.bind(this)} type="button" data-toggle="modal"
                                data-target="#exampleModal"  > {t('patient.dashboard.CreateNewPrescription')}
                        </button>
                        <button className={"patientsButtonDashboard"} onClick={this.openExisting.bind(this)} type="button" data-toggle="modal"
                                data-target="#exampleModal"  > {t('patient.dashboard.AddExistingPatient')}
                        </button>

                    </div>
                </div>


                <div className={'addPatientBoxTitleFilter'}>
                    {/*{t('doctor.all_patients')}*/}
                    <div className={'all-patients-search-wrapper'} >
                        <input className={"all-patients-search"} placeholder={view === 1? t('doctor.search_patient') : 'Search Prescription' } type="text" name="search"
                               id={"search"} onChange={event => this.props.filterPatientsTable('search',event.target.value)}/>
                        <HiOutlineSearch className={'bottomBorder inline'}/>
                    </div>
                    <button className={'noBorders'} onClick={this.openSideBar.bind(this)} type="button" data-toggle="modal"
                            data-target="#exampleModal" >
                            <FaFilter size={21} opacity={0.65} className={'inline'}/>
                    </button>
                </div>


                {
                    view === 1 ?
                    (
                        <PatientTable/>

                    )
                    : view === 2 ?
                    (
                        <PrescriptionsTable/>
                    )
                    : <div/>

                }
                {/*</div>*/}
                {isSideBarOpen && <Filter
                    title={'Open filter'}
                    filterPatientsTable={(type,payload) => this.props.filterPatientsTable(type,payload)}
                    isSideBarOpen={isSideBarOpen}
                    filters={this.props.patientsTableFilter.hasOwnProperty('filters') ? this.props.patientsTableFilter['filters'] : false}
                    closeSideBar={() => this.setState({isSideBarOpen: false})}/>}

                {isExistingPatientOpen && <ExistingPatient
                    title={'Edit Admin'}
                    isExistingPatientOpen={isExistingPatientOpen}
                    closeSideBar={() => this.setState({isExistingPatientOpen: false})}/>}

                {isPrescriptionOpen && <NewPrescription
                    title={'Edit Admin'}
                    isPrescriptionOpen={isPrescriptionOpen}
                    closeSideBar={() => this.setState({isPrescriptionOpen: false})}/>}
            </div>
        )
    }
}//
function mapDispatchToProps(dispatch) {
    return {
        filterPatientsTable: (type,payload) => dispatch(filterPatientsTable(type,payload))
    };
}
const mapStateToProps = state => {
    return {
        patientsTableFilter:state.patientsTableDataReducer.patientsTableFilter,
    };
};
const AllPatients = compose(
    withTranslation(),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(AllPatientsComposed);

export default AllPatients;