import React from 'react';
import {withTranslation} from "react-i18next";
import {compose} from "redux";


class ClinicalDashboardComposed extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
    }

    componentWillUnmount() {

    }

    render() {
        let {t} = this.props

       // return (
            // <div>
            //     <div className={'adminFonts'}> Clinical Data</div>
            //     <div className={'clinicalBoxes'}> ... </div>
            //     <div className={'clinicalBoxes'}> ... </div>
            // </div>

        return (
            <div >
                <div className={'clinicalDashboard'}>
                        <div className={'clinical-container'}>
                            <div className={'clinical-inner-container '}>
                                <label className={'adminFonts '}>{t('doctor.clinical_data')}</label>
                                <div className={'clinical-info-devices-status-info'}>
                                    <div className={`single-clinical-info-block `} >
                                        <label className={'single-clinical-info-block-value'}>1</label>
                                        <label className={'single-clinical-info-block-label'}>{t('doctor.reported_improvement')} </label>
                                    </div>
                                    <div className={`single-clinical-info-block`}>
                                        <label className={'single-clinical-info-block-value'}>3</label>
                                        <label className={'single-clinical-info-block-label'}>{t('doctor.reported_deterioration')}</label>
                                    </div>
                                    <div className={`single-clinical-info-block`}>
                                        <label className={'single-clinical-info-block-value'}>2</label>
                                        <label className={'single-clinical-info-block-label'}>{t('doctor.increased_drug_consumption')}</label>
                                    </div>
                                    <div className={`single-clinical-info-block`}>
                                        <label className={'single-clinical-info-block-value'}>2</label>
                                        <label className={'single-clinical-info-block-label'}>{t('doctor.increase_in_side_effects')} </label>
                                    </div>
                                    <div className={'single-clinical-info-block-middle'}/>

                                </div>
                            </div>
                            <div className={'clinical-info-container-right v-centered'}>
                                {/*<div className={'left-nova-info-block'}>*/}
                                    <label className={'adminFonts'}> {t('doctor.administrative_data')} </label>
                                    <div className={'clinical-info-devices-status-info'}>
                                        <div className={`single-clinical-info-block`} >
                                            <label className={'single-clinical-info-block-value'}>1</label>
                                            <label className={'single-clinical-info-block-label'}> {t('doctor.follow_up_is_needed')} </label>
                                        </div>
                                        <div className={`single-clinical-info-block`}>
                                            <label className={'single-clinical-info-block-value'}>3</label>
                                            <label className={'single-clinical-info-block-label'}>{t('doctor.poor_compliance')} </label>
                                        </div>
                                        <div className={`single-clinical-info-block `}>
                                            <label className={'single-clinical-info-block-value'}>2</label>
                                            <label className={'single-clinical-info-block-label'}>{t('doctor.IMCA_proccess')} I</label>
                                        </div>
                                    </div>
                                {/*</div>*/}
                            </div>
                        </div>
                </div>
                <div className={'nova-admin-dashboard container'}>

                </div>
                <div className={'nova-dashboard-chart'}>

                </div>
            </div>
        )
    }
}
const ClinicalDashboard = compose(
    withTranslation(),
    /*connect(
        mapStateToProps,
        mapDispatchToProps
    )*/
)(ClinicalDashboardComposed);

export default ClinicalDashboard;