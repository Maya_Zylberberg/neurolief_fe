import React, {Component} from 'react'
import { connect } from "react-redux";
import MyTable from "../../../../components/NovaTable/Table";
import {BsFillDropletFill} from "react-icons/bs";
import {RiPlantLine} from "react-icons/ri";
import {AiFillWarning} from "react-icons/ai";
import {BiDotsVertical} from "react-icons/bi";
import {compose} from "redux";
import {withTranslation} from "react-i18next";

import {history} from "../../../../utils/history";


import {setPatientsData} from "../../../../redux/actions/Tabs-Actions";
import utils from "../../../../utils/utils";
// import {adminApi} from "../../../services/ApiService";
// import AlertConfirm from "../../../components/modals/Confirm";
// import VersionEditForm from "../../../components/version/VersionEditForm";
// import {makeError} from "../../../components/modals/ErrorModal";
// import SVGIcons from "../../../assets/SVGIcons";


class PatientTableComposed extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visibleCollection: [],
            filteredCollection: [],
            isMore: false
        }
    }

    async openPatientInfoPage(){

        history.push({
            pathname: '/dashboard/Patient',
            state: {
                /*
                                Email:this.state.username,
                */
                from: {
                    fromPath: `/dashboard`,
                }
            },
        })
    }


    setTempData = () => {
        let data = [
            {
                PatientName:"Maya Zylberberg",
                ID:323443092,
                Sex: "Female",
                Age:"24",
                TreatmentStartDate: "2023-03-01T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2023-03-01T14:48:00.000Z"),
                LastVisit:'11/11/20',
                LastVisitVal: new Date("11/11/20"),
                Medications:['Opioids']


            },
            {
                PatientName:"subhi samara",
                ID:123456789,
                Sex: "Male",
                Age:"28",
                TreatmentStartDate: "2021-01-01T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2021-01-01T14:48:00.000Z"),
                LastVisit:'10/10/20',
                LastVisitVal: new Date("10/10/20"),
                Medications:['Opioids','Tritans']


            },
            {
                PatientName:"subhi2 samara2",
                ID:987654321,
                Sex: "Male",
                Age:"22",
                TreatmentStartDate: "2021-01-02T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2021-01-02T14:48:00.000Z"),
                LastVisit:'10/10/20',
                LastVisitVal: new Date("10/10/20"),
                Medications:['Tritans']


            },
            {
                PatientName:"Maya Zylberberggg",
                ID:564443092,
                Sex: "Female",
                Age:"26",
                TreatmentStartDate: "2019-03-01T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2019-03-01T14:48:00.000Z"),
                LastVisit:'11/11/20',
                LastVisitVal: new Date("11/11/20"),
                Medications:['Opioids']
            },
            {
                PatientName:"ddd ggeeewwdr",
                ID:323443092,
                Sex: "Male",
                Age:"18",
                TreatmentStartDate: "2019-03-01T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2019-03-01T14:48:00.000Z"),
                LastVisit:'18/9/20',
                LastVisitVal: new Date("18/9/20"),
                Medications:['Tritans']

            },
            {
                PatientName:"ggd fknd",
                ID:543443092,
                Sex: "Male",
                Age:"28",
                TreatmentStartDate: "2024-01-01T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2024-01-01T14:48:00.000Z"),
                LastVisit:'02/11/20',
                LastVisitVal: new Date("02/11/20"),
                Medications:['Opioids','Tritans']

            },

            {
                PatientName:"Maya Zylberberggg",
                ID:564443092,
                Sex: "Female",
                Age:"26",
                TreatmentStartDate: "2019-03-12T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2019-03-12T14:48:00.000Z"),
                LastVisit:'11/11/20',
                LastVisitVal: new Date("11/11/20"),
                Medications:['Opioids']
            },
            {
                PatientName:"ddd ggeeewwdr",
                ID:323443092,
                Sex: "Male",
                Age:"18",
                TreatmentStartDate: "2019-03-01T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2019-03-01T14:48:00.000Z"),
                LastVisit:'18/9/20',
                LastVisitVal: new Date("18/9/20"),
                Medications:['Opioids','Tritans']

            },
            {
                PatientName:"ggd fknd",
                ID:543443092,
                Sex: "Male",
                Age:"28",
                TreatmentStartDate: "2024-01-01T14:48:00.000Z",
                TreatmentStartDateVal: new Date("2024-01-01T14:48:00.000Z"),
                LastVisit:'02/11/20',
                LastVisitVal: new Date("02/11/20"),
                Medications:['Tritans']

            }
        ]
        this.props.setPatientsData(data)
    }

    getFields = (t) => {
        return [
            {accessor: 'PatientName', Header: t('doctor.table.PatientName'), makeFilter: false, resizable: false, Cell: ({original}) => {
                   return(
                    <div onClick={this.openPatientInfoPage} style={{cursor: 'pointer'}}>
                        {original.PatientName}
                    </div>
                   ) }},
            {accessor: 'ID', Header: t('doctor.table.ID'), resizable: false },
            {accessor: 'Sex', Header: t('doctor.table.Sex'), resizable: false},
            {accessor: 'Age', Header: t('doctor.table.Age'), resizable: false},
            {accessor: 'TreatmentStartDate', Header:  t('doctor.table.TreatmentStartDate') , resizable: false, Cell: ({original}) => {
                return utils.getDateFormatWithYear(new Date(original.TreatmentStartDate))
            }},

            {accessor: 'LastVisit', Header: t('doctor.table.LastVisit'), resizable: false},
            {accessor: 'Medications', Header: t('doctor.table.Medications'), resizable: false
                // ,Cell: ({ original }) => {
                //     if (original['Medications'].size > 1)
                //         return (
                //             <div>
                //                 Tritans , Opioids
                //             </div>
                //         );
                    // else {
                    //     return {original}
                    // }
                // }
                },

            {accessor: 'options', Header: ' ', resizable: false, maxWidth:20
                ,Cell: ({ original }) => {
                    return (<BiDotsVertical size={30} color={'gray'}/>);

                }}

        ]
    }

    componentDidMount() {
        this.setTempData()
    }

    //
    // async openSideBar(version){
    //     document.body.style.overflow = "hidden"
    //     await this.setState({entry:version,isSideBarOpen:true})
    //     this.forceUpdate();
    // }
    //
    // componentDidMount = async () : void => {
    //     await this.setState({
    //         visibleCollection: this.props.versionData,
    //         filteredCollection: this.props.versionData,
    //     })
    // }
    //
    // componentWillUnmount(): void {
    //     this.props.cleanVersion()
    // }
    //
    // onSelectionChange = (selected) => {
    //     this.setState({selected})
    //     this.props.setVersionDataSelection(selected)
    // }
    //
    // editVersionDetails = async (versions) => {
    //     await this.openSideBar(versions)
    // }
    //
    // removeVersion = async (row) => {
    //     let confirm = await AlertConfirm({
    //         options:{
    //             title: `Delete Version`,
    //         }},`Are you sure you want to delete this Version: ${row.VersionNO} of ${row.Type}?`)
    //     if (confirm) {
    //         let SystemVersionID = row.SystemVersionID
    //         let response = await adminApi.deleteVersion(SystemVersionID)
    //         if (response){
    //             await this.props.deleteVersion(response.data.SystemVersionID)
    //         }
    //     }
    // }
    //
    // setVersionsMoreFunctions = () => {
    //     return [
    //         {
    //             name: 'Edit Version',
    //             call: (row => this.editVersionDetails(row))
    //         },
    //         {
    //             name: 'Remove Version',
    //             call: (row => this.removeVersion(row))
    //         }
    //     ]
    // }
    //
    // onFilterDataSelected = (filter) => {
    //     this.props.filterVersionsTable(Object.keys(filter)[0],Object.values(filter)[0])
    // }
    changeIsMore = () => {
        let val = !this.state.isMore
        this.setState({isMore: val} );
    }

    render() {
        let {t} = this.props

        let AllowedFields = this.getFields(t)
        let {entry,isSideBarOpen} = this.state
        return (
            <div className={'boxes'}>
                <div className={'context-area'}>
                    <MyTable
                        // data={this.state.isMore ? this.props.patientsData : this.props.patientsData.slice(0,8)}
                        data={ this.props.patientsData}
                        initialData={/*this.props.initiateVersionData*/[]}
                        columns={AllowedFields}
                    />
                </div>
               {/* <div className={'buttonBox'}>
                    <button className={'button6'} onClick={this.changeIsMore} > {t('doctor.show_more')} </button>
                </div>*/}
                <div className={'bottomSpace'}/>
            </div>
        )
    }
}
//
function mapDispatchToProps(dispatch) {
    return {
        setPatientsData: patients => dispatch(setPatientsData(patients))
    };
}
const mapStateToProps = state => {
    return {
        patientsData:state.patientsTableDataReducer.patientsData,
        initiatePatientsData:state.patientsTableDataReducer.initiatePatientsData
    };
};
//

const PatientTable = compose(
    withTranslation(),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(PatientTableComposed);

export default PatientTable;
