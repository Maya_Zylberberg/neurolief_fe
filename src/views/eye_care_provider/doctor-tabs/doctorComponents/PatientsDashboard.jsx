import React from 'react';
import { FaOilCan } from 'react-icons/fa';
import { RiPlantLine } from 'react-icons/ri';
import { BsFillDropletFill } from 'react-icons/bs';
import {Nav,Navbar,NavDropdown} from "react-bootstrap";
import {Dropdown} from 'react-bootstrap'
import TreatmentCompliance from '../../../../components/doctor/PatientsDropdown/TreatmentComplience';
import Flags from '../../../../components/doctor/PatientsDropdown/Flags';
import ClinicalGlobalImprovement from '../../../../components/doctor/PatientsDropdown/ClinicalGlobalImprovement';
import DeliverySystem from '../../../../components/doctor/PatientsDropdown/DeliverySystem';
import {withTranslation} from "react-i18next";
import {compose} from "redux";
import {connect} from "react-redux";
import AddPatient from "../../../../components/doctor/addPatient";





class PatientsDashboardComposed extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            isSideBarOpen:false
        }
    }

    componentDidMount() {
    }

    componentWillUnmount() {

    }



    openSideBar(){
        document.body.style.overflow = "hidden"
        this.setState({isSideBarOpen:true})
    }




    render() {
        let {t} = this.props
        let {isSideBarOpen} = this.state

        return (

            <div>
                <div className={'firstPatientBox'}>
                        <button className={"button5 buttonMargin"} onClick={this.openSideBar.bind(this)} type="button" data-toggle="modal"
                                data-target="#exampleModal"  >+ {t('doctor.add_new_patients')}
                        </button>
                </div>

                {isSideBarOpen && <AddPatient
                    title={'Edit Admin'}
                    isSideBarOpen={isSideBarOpen}
                    closeSideBar={() => this.setState({isSideBarOpen: false})}/>}
           </div>
        );
    }
}
const PatientsDashboard = compose(
    withTranslation(),
    /*connect(
        mapStateToProps,
        mapDispatchToProps
    )*/
)(PatientsDashboardComposed);
export default PatientsDashboard;