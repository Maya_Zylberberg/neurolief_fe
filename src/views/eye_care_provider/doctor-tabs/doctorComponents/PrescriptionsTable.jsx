import React, {Component} from 'react'
import { connect } from "react-redux";
import MyTable from "../../../../components/NovaTable/Table";

import {AiOutlineMail, AiOutlineFilePdf} from "react-icons/ai";
import {BiDotsVertical} from "react-icons/bi";
import {compose} from "redux";
import {withTranslation} from "react-i18next";

import {history} from "../../../../utils/history";


import {setPatientsData} from "../../../../redux/actions/Tabs-Actions";
import utils from "../../../../utils/utils";
// import {adminApi} from "../../../services/ApiService";
// import AlertConfirm from "../../../components/modals/Confirm";
// import VersionEditForm from "../../../components/version/VersionEditForm";
// import {makeError} from "../../../components/modals/ErrorModal";
// import SVGIcons from "../../../assets/SVGIcons";


class PatientTableComposed extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visibleCollection: [],
            filteredCollection: [],
            isMore: false
        }
    }

    async openPatientInfoPage(){

        history.push({
            pathname: '/dashboard/Patient',
            state: {
                /*
                                Email:this.state.username,
                */
                from: {
                    fromPath: `/dashboard`,
                }
            },
        })
    }


    setTempData = () => {
        let data = [
            {
                Number: 2255645544,
                IssueDate: "2023-03-01T14:48:00.000Z",
                IssueDateVal: new Date("2023-03-01T14:48:00.000Z"),
                PatientName:"Maya Zylberberg",
                ID:323443092
            },
            {
                Number: 1325336,
                IssueDate: "2021-01-01T14:48:00.000Z",
                IssueDateVal: new Date("2021-01-01T14:48:00.000Z"),
                PatientName:"subhi samara",
                ID:123456789
            },
            {
                Number: 66334754,
                IssueDate: "2021-01-02T14:48:00.000Z",
                IssueDateVal: new Date("2021-01-02T14:48:00.000Z"),
                PatientName:"subhi2 samara2",
                ID:987654321,

            },
            {
                Number: 1357642,
                IssueDate: "2019-03-01T14:48:00.000Z",
                IssueDateVal: new Date("2019-03-01T14:48:00.000Z"),
                PatientName:"Maya Zylberberggg",
                ID:564443092,
            },
            {
                Number: 92325443,
                IssueDate: "2019-03-01T14:48:00.000Z",
                IssueDateVal: new Date("2019-03-01T14:48:00.000Z"),
                PatientName:"ddd ggeeewwdr",
                ID:323443092,
            },
            {
                Number: 24642467667,
                IssueDate: "2024-01-01T14:48:00.000Z",
                IssueDateVal: new Date("2024-01-01T14:48:00.000Z"),
                PatientName:"ggd fknd",
                ID:543443092,
            },

            {
                Number: 125345544,
                IssueDate: "2019-03-12T14:48:00.000Z",
                IssueDateVal: new Date("2019-03-12T14:48:00.000Z"),
                PatientName:"Maya Zylberberggg",
                ID:564443092,

            },
            {
                Number: 8655645544,
                IssueDate: "2019-03-01T14:48:00.000Z",
                IssueDateVal: new Date("2019-03-01T14:48:00.000Z"),
                PatientName:"ddd ggeeewwdr",
                ID:323443092,
            },
            {
                Number: 4433245544,
                IssueDate: "2024-01-01T14:48:00.000Z",
                IssueDateVal: new Date("2024-01-01T14:48:00.000Z"),
                PatientName:"ggd fknd",
                ID:543443092,
            }
        ]
        this.props.setPatientsData(data)
    }

    getFields = (t) => {
        return [
            {accessor: 'Number', Header: t('doctor.table.Number'), resizable: false},
            {accessor: 'IssueDate', Header:  t('doctor.table.IssueDate')  , resizable: false, Cell: ({original}) => {
                    return utils.getDateFormatWithYear(new Date(original.IssueDate))
                }},
            // {accessor: 'IssueDate', Header: 'IssueDate', resizable: false },
            {accessor: 'PatientName', Header: t('doctor.table.PatientName'), resizable: false},
            {accessor: 'ID', Header:  t('doctor.table.ID') , resizable: false},


            {accessor: 'Purchased', Header:  t('doctor.table.Purchased') , resizable: false},

            {accessor: 'Mail', Header: '', resizable: false, width: 40
                ,Cell: ({ original }) => {
                    return (<AiOutlineMail size={30} color={'gray'}/>);

                }},
            {accessor: 'PDF', Header: '', resizable: false, width: 40
                ,Cell: ({ original }) => {
                    return (<AiOutlineFilePdf size={30} color={'gray'}/>);

                }}








            /*,

            {accessor: 'Medications', Header: t('doctor.table.Medications'), resizable: false
                // ,Cell: ({ original }) => {
                //     if (original['Medications'].size > 1)
                //         return (
                //             <div>
                //                 Tritans , Opioids
                //             </div>
                //         );
                // else {
                //     return {original}
                // }
                // }
            },

            {accessor: 'options', Header: ' ', resizable: false, maxWidth:20
                ,Cell: ({ original }) => {
                    return (<BiDotsVertical size={30} color={'gray'}/>);

                }}*/
        ]
    }

    componentDidMount() {
        this.setTempData()
    }

    //
    // async openSideBar(version){
    //     document.body.style.overflow = "hidden"
    //     await this.setState({entry:version,isSideBarOpen:true})
    //     this.forceUpdate();
    // }
    //
    // componentDidMount = async () : void => {
    //     await this.setState({
    //         visibleCollection: this.props.versionData,
    //         filteredCollection: this.props.versionData,
    //     })
    // }
    //
    // componentWillUnmount(): void {
    //     this.props.cleanVersion()
    // }
    //
    // onSelectionChange = (selected) => {
    //     this.setState({selected})
    //     this.props.setVersionDataSelection(selected)
    // }
    //
    // editVersionDetails = async (versions) => {
    //     await this.openSideBar(versions)
    // }
    //
    // removeVersion = async (row) => {
    //     let confirm = await AlertConfirm({
    //         options:{
    //             title: `Delete Version`,
    //         }},`Are you sure you want to delete this Version: ${row.VersionNO} of ${row.Type}?`)
    //     if (confirm) {
    //         let SystemVersionID = row.SystemVersionID
    //         let response = await adminApi.deleteVersion(SystemVersionID)
    //         if (response){
    //             await this.props.deleteVersion(response.data.SystemVersionID)
    //         }
    //     }
    // }
    //
    // setVersionsMoreFunctions = () => {
    //     return [
    //         {
    //             name: 'Edit Version',
    //             call: (row => this.editVersionDetails(row))
    //         },
    //         {
    //             name: 'Remove Version',
    //             call: (row => this.removeVersion(row))
    //         }
    //     ]
    // }
    //
    // onFilterDataSelected = (filter) => {
    //     this.props.filterVersionsTable(Object.keys(filter)[0],Object.values(filter)[0])
    // }
    changeIsMore = () => {
        let val = !this.state.isMore
        this.setState({isMore: val} );
    }

    render() {
        let {t} = this.props

        let AllowedFields = this.getFields(t)
        let {entry,isSideBarOpen} = this.state
        return (
            <div className={'boxes'}>
                <div className={'context-area'}>
                    <MyTable
                        // data={this.state.isMore ? this.props.patientsData : this.props.patientsData.slice(0,8)}
                        data={ this.props.patientsData}
                        initialData={/*this.props.initiateVersionData*/[]}
                        columns={AllowedFields}
                    />
                </div>
                {/* <div className={'buttonBox'}>
                    <button className={'button6'} onClick={this.changeIsMore} > {t('doctor.show_more')} </button>
                </div>*/}
                <div className={'bottomSpace'}/>
            </div>
        )
    }
}
//
function mapDispatchToProps(dispatch) {
    return {
        setPatientsData: patients => dispatch(setPatientsData(patients))
    };
}
const mapStateToProps = state => {
    return {
        patientsData:state.patientsTableDataReducer.patientsData,
        initiatePatientsData:state.patientsTableDataReducer.initiatePatientsData
    };
};
//

const PatientTable = compose(
    withTranslation(),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(PatientTableComposed);

export default PatientTable;
