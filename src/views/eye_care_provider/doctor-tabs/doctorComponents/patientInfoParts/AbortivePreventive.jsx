import React, {Component} from 'react'
// import {history} from "../../utils/history";
//
// import {connect} from "react-redux";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import Abortive from "../../../../../components/doctor/PatientInfo/Abortive";
import Preventive from "../../../../../components/doctor/PatientInfo/Preventive";
import { AiOutlineCheckCircle } from 'react-icons/ai';



class AbortivePreventiveComposed /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            view: 1
        }
    }

    componentDidMount() {
        // this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }
    changeView1 = () => {
        this.setState({view: 1});
    }

    changeView2 = () => {
        this.setState({view: 2});
    }

    render() {
        let {data} = this.props
        let { view } = this.state

        return (
            <div>
                <div className={'AbortivePreventiveMainBox'}>
                    <div className={view === 1 ? 'abortiveClicked': 'abortiveNotClicked'}  onClick={this.changeView1} >Abortive</div>
                    <div className={view === 2 ? 'abortiveClicked': 'abortiveNotClicked'}  onClick={this.changeView2} >Preventive </div>

                </div>

                {
                    view === 1 ?
                        (
                            <Abortive/>
                        )
                        : view === 2 ?
                        (
                            <Preventive/>
                        )
                        : <div/>

                }

                <div className={'patientInfoFont'}>
                    Relivion System Insights
                </div>
                <div className={'relivionBox'}>
                    <AiOutlineCheckCircle size={45} opacity={0.45} color={'white'}/> <div className={'relivionBoxInside'}>  Patient reported most migraines usually on the weekends</div>
                    <AiOutlineCheckCircle size={45} opacity={0.45} color={'white'}/><div className={'relivionBoxInside'}> Patient's treatment results are better when lying down</div>
                    <AiOutlineCheckCircle size={45} opacity={0.45} color={'white'}/>  <div className={'relivionBoxInside'}> Patient reported most migraines usually on the weekends</div>
                </div>

            </div>
        )

    }
}
const AbortivePreventive = compose(
    withTranslation(),
    /* connect(
         mapStateToProps,
         mapDispatchToProps
     )*/
)(AbortivePreventiveComposed);

export default AbortivePreventive;
