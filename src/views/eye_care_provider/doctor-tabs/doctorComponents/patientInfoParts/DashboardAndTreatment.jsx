import React, {Component} from 'react'
// import {history} from "../../utils/history";
//
// import {connect} from "react-redux";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import { MdExpandLess, MdExpandMore } from 'react-icons/md';
import { BiHistory } from 'react-icons/bi';
import TreatmentRegimen from "../../../../../components/doctor/PatientInfo/TreatmentRegimen";
import TreatmentHistory from "../../../../../components/doctor/PatientInfo/TreatmentHistory";
import {history} from "../../../../../utils/history";


class DashboardAndTreatmentComposed /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "title",
            Expand: MdExpandLess,
            clicked: false,
            view: 1


        }
    }

    componentDidMount() {
        // this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }

    changeView1 = () => {
        this.setState({view: 1});
    }

    changeView2 = () => {
        this.setState({view: 2});
    }
    async BackToDashboard(){

        history.push({
            pathname: '/dashboard',
            state: {
                /*
                                Email:this.state.username,
                */
                from: {
                    fromPath: `/dashboard/Patient`,
                }
            },
        })
    }

    expandFilter(id){
        let display = document.getElementById(id).style.display
        let clicked = this.state.clicked

        if (display === "block") {
            display = "none"
            this.setState({Expand: MdExpandLess})
        }
        else {
            display = "block"
            this.setState({Expand: MdExpandMore})
        }
        this.setState({clicked: !clicked})

        // if (display === "block"){
        //     this.setState({Expand: MdExpandLess})
        // } else {
        //     this.setState({Expand: MdExpandMore})
        // }
        document.getElementById(id).style.display = display;

    }

    render() {
        let {data} = this.props
        let {Expand, clicked} = this.state
        let { view } = this.state
        let {t} = this.props



        return (
                <div>
                    <div className={!clicked ? 'titleBox' : 'titleBoxExpanded'}>
                        <div className={'adminMargin'}>
                            <div className={'dashboardBackButton'} onClick={this.BackToDashboard}> &lt; Back </div>
                            <div className={'dashboardTitleWrapper'}>
                                <div className={'titleBoxInsideBig'} >
                                    <div className={ 'titleBoxInside1'}>
                                        <div className={'titleBoxBigFont'}> Alex Avraham </div>
                                        <div className={'titleBoxSmallFont'}>  {t('doctor.patientInfo.Name')}  </div>
                                    </div>
                                    <div className={ 'titleBoxInside2'} style={{display:  !clicked ?'inline-block': 'none'}}>
                                        <div className={'titleBoxBigFont'}> 335435532 </div>
                                        <div className={'titleBoxSmallFont'}>  {t('doctor.patientInfo.ID')}  </div>
                                    </div>
                                    <div className={ 'titleBoxInside3'} style={{display:  !clicked ?'inline-block': 'none'}}>
                                        <div className={'titleBoxBigFont'}> 26 </div>
                                        <div className={'titleBoxSmallFont'}>  {t('doctor.patientInfo.Age')}  </div>
                                    </div>

                                </div>
                                <div className={'titleBoxRight'}  onClick={() => this.expandFilter('panel')}>
                                    Personal Details
                                    <Expand className={'expandingIcon pointer'} size={35} />
                                </div>
                            </div>
                            <div id={'panel'} style={{display: "none"}}>
                                <div className={'titleBoxInsideBigExpanded'} >
                                    <div className={'titleBoxInsideExpanded'}>{t('doctor.patientInfo.ID')}: 335435532</div>
                                    <div className={'titleBoxInsideExpanded'}>{t('doctor.patientInfo.Age')} : 26</div>
                                    <div className={'titleBoxInsideExpanded'}>{t('doctor.patientInfo.sex')} : Male</div>
                                    <div className={'titleBoxInsideExpanded'}>{t('doctor.patientInfo.Medications')} : Opioids, Tritans</div>
                                    <div className={'titleBoxInsideExpanded'}>{t('doctor.patientInfo.TreatmentStartDate')}  02/11/2019</div>
                                    <div className={'PersonalDetailsButton'}>{t('doctor.patientInfo.UpdatePatientInformation')} </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div className={'adminMargin'}>

                        <div className={'treatmentRegimenBox'}>
                                <div className={'patientRegimenTitleBox'}>
                                    <div className={'patientInfoFont'}  onClick={this.changeView1}>{t('doctor.patientInfo.TreatmentRegimen')} </div>
                                    <div className={'patientRegimenTitleRightBox'}>
                                        <div className={'regimenHistoryFont'}  onClick={this.changeView2}><BiHistory size={24} color={'#0575e6'}/>{t('doctor.patientInfo.History')}  </div>
                                        <div className={'RegimenButton'}>{t('doctor.patientInfo.UpdateTreatmentRegimen')} </div>
                                    </div>
                                </div>

                                {
                                    view === 1 ?
                                        (
                                            <TreatmentRegimen/>
                                        )
                                        : view === 2 ?
                                        (
                                            <TreatmentHistory/>
                                        )
                                        : <div/>
                                }
                            </div>
                    </div>
            </div>
    )

    }
}
const DashboardAndTreatment = compose(
    withTranslation(),
    /* connect(
         mapStateToProps,
         mapDispatchToProps
     )*/
)(DashboardAndTreatmentComposed);

export default DashboardAndTreatment;
