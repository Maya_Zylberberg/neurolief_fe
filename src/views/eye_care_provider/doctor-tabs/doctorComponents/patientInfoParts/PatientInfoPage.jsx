import React, {Component} from 'react'
// import {history} from "../../utils/history";
//
// import {connect} from "react-redux";
// import {filterPatientsTable} from "../../redux/actions/Tabs-Actions";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import {history} from "../../../../../utils/history";
import '../../../../../components_style/doctorComponents/PatientsInfo.css'
import DashboardAndTreatment from './DashboardAndTreatment'
import AbortivePreventive from './AbortivePreventive'
import PatientsStatistics from "./PatientsStatistics";


class PatientInfoPageComposed /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "title",

        }
    }

    componentDidMount() {
        // this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }



    // new commit text
    render() {
        let {data} = this.props
        return (
            <div className={'totalBackground '}>
                <DashboardAndTreatment/>
                <div className={'adminMargin'}>
                    <AbortivePreventive/>
                    <PatientsStatistics/>
                </div>
            </div>
        )
    }
}
const PatientInfoPage = compose(
    withTranslation(),
   /* connect(
        mapStateToProps,
        mapDispatchToProps
    )*/
)(PatientInfoPageComposed);

export default PatientInfoPage;
