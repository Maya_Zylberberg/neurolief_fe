import React, {Component} from 'react'
// import {history} from "../../utils/history";
//
// import {connect} from "react-redux";
import {compose} from "redux";
import {withTranslation} from "react-i18next";
import HealthInformation from "../../../../../components/doctor/PatientInfo/HealthInformation";
import PatientStatistics from "../../../../../components/doctor/PatientInfo/PatientStatistics";
import TreatmentCalendar from "../../../../../components/doctor/PatientInfo/TreatmentCalendar";

import { AiOutlineCheckCircle } from 'react-icons/ai';



class PatientsStatisticsComposed /*ConComposed*/ extends Component {

    constructor(props) {
        super(props);
        this.state = {
            view: 1
        }
    }

    componentDidMount() {
        // this.setState({title:"new Admin"})
    }

    componentWillUnmount() {

    }
    changeView1 = () => {
        this.setState({view: 1});
    }

    changeView2 = () => {
        this.setState({view: 2});
    }


    changeView3 = () => {
        this.setState({view: 3});
    }

    render() {
        let {data} = this.props
        let { view } = this.state

        return (
            <div>
                <div className={'patientInfoFont'}>
                    Patient Statistics (Last 3 Month)
                </div>
                <div className={'statisticsMainBox'}>
                    <div className={view === 1 ? 'StatisticsClicked': 'StatisticsNotClicked'}  onClick={this.changeView1} >Treatment Calendar</div>
                    <div className={view === 2 ? 'StatisticsClicked': 'StatisticsNotClicked'}  onClick={this.changeView2} >PatientStatistics </div>
                    <div className={view === 3 ? 'StatisticsClicked': 'StatisticsNotClicked'}  onClick={this.changeView3} >Health information </div>


                </div>

                {
                    view === 1 ?
                    (
                        <TreatmentCalendar/>
                    )
                    : view === 2 ?
                    (
                        <PatientStatistics/>
                    )
                    : view === 3 ?
                    (
                        <HealthInformation/>
                    )
                    : <div/>

                }

            </div>
        )

    }
}
const PatientsStatistics = compose(
    withTranslation(),
    /* connect(
         mapStateToProps,
         mapDispatchToProps
     )*/
)(PatientsStatisticsComposed);

export default PatientsStatistics;
